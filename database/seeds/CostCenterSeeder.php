<?php

use Illuminate\Database\Seeder;
use App\CostCenter;

class CostCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project1 = CostCenter::FirstOrNew([
            'type' => 'IT',
            'number' =>  0123,
            'user_id' => 1,
        ]);
        $project1->save();

        $project2 = CostCenter::FirstOrNew([
            'type' => 'Graphics',
            'number' =>  4682,
            'user_id' => 1,
        ]);
        $project2->save();

        $project3 = CostCenter::FirstOrNew([
            'type' => 'Language',
            'number' =>  7832,
            'user_id' => 2,
            'user_id' => 1,
        ]);
        $project3->save();

        $project4 = CostCenter::FirstOrNew([
            'type' => 'Communications',
            'number' =>  9999,
            'user_id' => 2,
        ]);
        $project4->save();

        $project5 = CostCenter::FirstOrNew([
            'type' => 'Marketing',
            'number' =>  5471,
        ]);
        $project5->save();
    }
}
