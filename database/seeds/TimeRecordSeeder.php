<?php

use Illuminate\Database\Seeder;
use App\TimeRecord;
use Carbon\Carbon;
use App\TimeSheet;
use App\CostCenter;
use App\User;
use App\Grade;

class TimeRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();


        $timeSample = TimeSheet::firstOrNew([

        ]);


        for($month=0; $month<6; $month++){
            // get the 
            $timeSheet = TimeSheet::firstOrNew([
                'month' => Carbon::parse('2020' . '-' . '0' . $month. '-' . '01')->format('F'),
                'year' => 2020,
                'user_id' => 1,
                'company' => 'Belfast Met'
            ]);
            $timeSheet->save();

            
            
            for($i = 0; $i <15; $i++){
                if($month < 9){
                    echo($month);
                    $dt1 =  Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '2020-'. '0' . $month . '-01', $endDate = '2020-'. '0' . $month . '-30')->getTimeStamp()) ;
                }else{
                    echo('month in else' . $month);
                    $dt1 = Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '2020-'. $month . '-01', $endDate = '2020-'. '0' . $month . '-30')->getTimeStamp()) ;
                }
                $timeIn = $dt1->setTime(9,0);
                $timeOut = Carbon::createFromFormat('Y-m-d H:i:s', $timeIn)->addHours( $faker->numberBetween( 1, 8 ) );
                $hoursWorked = TimeRecord::hoursWorked($timeIn, $timeOut);

                TimeRecord::create([
                    'time_sheet_id' => $timeSheet->id,
                    'cost_center_id' => rand(1, CostCenter::count()),
                    'user_id' => rand(1, User::count()),
                    'grade_id' => rand(1, Grade::count()),
                    'timeIn' => $timeIn,
                    'timeOut' => $timeOut,
                    'break' => $faker->boolean($chanceOfGettingTrue = 50),
                    'subject' => $faker->text($maxNbChars = 10),
                    'description' => $faker->text($maxNbChars = 100),
                    'dateOfRecord' => $dt1->format("Y-m-d"),
                    'minutes' => $hoursWorked*60,
                ]);
            }//for
        }//for

        TimeRecord::create([
            'time_sheet_id' => 1,
            'cost_center_id' => rand(1, CostCenter::count()),
            'user_id' => rand(1, User::count()),
            'grade_id' => 3,
            'timeIn' => $timeIn,
            'timeOut' => $timeOut,
            'break' => $faker->boolean($chanceOfGettingTrue = 50),
            'subject' => 'Java',
            'description' => $faker->text($maxNbChars = 100),
            'dateOfRecord' => Carbon::parse('2020-01-20')->format("Y-m-d"),
            'minutes' => $hoursWorked*60,
        ]);

        TimeRecord::create([
            'time_sheet_id' => 1,
            'cost_center_id' => rand(1, CostCenter::count()),
            'user_id' => rand(1, User::count()),
            'grade_id' => 3,
            'timeIn' => $timeIn,
            'timeOut' => $timeOut,
            'break' => $faker->boolean($chanceOfGettingTrue = 50),
            'subject' => 'Java',
            'description' => $faker->text($maxNbChars = 100),
            'dateOfRecord' => Carbon::parse('2020-01-06')->format("Y-m-d"),
            'minutes' => $hoursWorked*60,
        ]);

        TimeRecord::create([
            'time_sheet_id' => 1,
            'cost_center_id' => rand(1, CostCenter::count()),
            'user_id' => rand(1, User::count()),
            'grade_id' => rand(1, Grade::count()),
            'timeIn' => $timeIn,
            'timeOut' => $timeOut,
            'break' => $faker->boolean($chanceOfGettingTrue = 50),
            'subject' => 'Java',
            'description' => $faker->text($maxNbChars = 100),
            'dateOfRecord' => Carbon::parse('2020-01-13')->format("Y-m-d"),
            'minutes' => $hoursWorked*60,
        ]);
    }
}
