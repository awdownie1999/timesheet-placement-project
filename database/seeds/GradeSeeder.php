<?php

use Illuminate\Database\Seeder;
use App\Grade;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grade3 = Grade::firstOrNew([
            'type' => 3,
        ]);
        $grade3->save();
    
        $grade4 = Grade::firstOrNew([
            'type' => 4,
        ]);
        $grade4->save();

        $grade5 = Grade::firstOrNew([
            'type' => 5,
        ]);
        $grade5->save();
    }
}
