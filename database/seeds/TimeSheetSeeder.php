<?php

use Illuminate\Database\Seeder;
use App\TimeSheet;
use Carbon\Carbon;

class TimeSheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jan = TimeSheet::firstOrNew([
            'month' => 'January',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'company' => 'Belfast Met',
            'strand' => 1
        ]);
        $jan->save();

        $feb = TimeSheet::firstOrNew([
            'month' => 'February',
            'name' => 'MyTimeSheet2',
            'year' => 2020,
            'user_id' => 1,
            'company' => 'Belfast Met',
            'strand' => 1
        ]);
        $feb->save();

        $mar = TimeSheet::firstOrNew([
            'month' => 'March',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'company' => 'Belfast Met',
            'strand' => 1
        ]);
        $mar->save();

        $apr = TimeSheet::firstOrNew([
            'month' => 'April',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $apr->save();


        $may = TimeSheet::firstOrNew([
            'month' => 'May',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $may->save();

        $jun = TimeSheet::firstOrNew([
            'month' => 'June',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $jun->save();

        $jul = TimeSheet::firstOrNew([
            'month' => 'July',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $jul->save();

        $aug = TimeSheet::firstOrNew([
            'month' => 'August',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $aug->save();

        $sep = TimeSheet::firstOrNew([
            'month' => 'September',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $sep->save();

        $oct = TimeSheet::firstOrNew([
            'month' => 'October',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $oct->save();

        $nov = TimeSheet::firstOrNew([
            'month' => 'November',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $nov->save();

        $dec = TimeSheet::firstOrNew([
            'month' => 'December',
            'name' => 'MyTimeSheet1',
            'year' => 2020,
            'user_id' => 1,
            'strand' => 1,
            'company' => 'Belfast Met'
        ]);
        $dec->save();

    }
}