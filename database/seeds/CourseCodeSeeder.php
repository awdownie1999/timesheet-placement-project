<?php

use Illuminate\Database\Seeder;
use App\CourseCode;

class CourseCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code1 = CourseCode::FirstOrNew([
            'name' =>  'Software',
            'number' =>  4013,
            'user_id' => 1,
        ]);
        $code1->save();

        $code2 = CourseCode::FirstOrNew([
            'name' =>  'Communication',
            'number' =>  0123,
            'user_id' => 1,
        ]);
        $code2->save();

        $code3 = CourseCode::FirstOrNew([
            'name' =>  'Marketing',
            'number' =>  7832,
            'user_id' => 1,
        ]);
        $code3->save();

        $code4 = CourseCode::FirstOrNew([
            'name' =>  'Business',
            'number' =>  9999,
            'user_id' => 1,
        ]);
        $code4->save();

        $code5 = CourseCode::FirstOrNew([
            'name' =>  'Health & Social',
            'number' =>  5471,
            'user_id' => 2,
        ]);
        $code5->save();
    }
}
