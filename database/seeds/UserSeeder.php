<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testUser = User::firstOrNew([
            'name' => 'test',
            'email' => 'test@belfastmet.ac.uk',
            'password' => Hash::make('password'),
            'employee_number' => 9014
        ]);
        $testUser->save();


        $user2 = User::firstOrNew([
            'name' => 'Aoife',
            'email' => 'aoife@belfastmet.ac.uk',
            'password' => Hash::make('password'),
            'employee_number' => 0110
        ]);
        $user2->save();
    }
}
