<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//tests
Route::get('/timesheet', 'TimeSheetController@testing');
Route::get('/pdfdwl', 'TimeSheetController@generateTimeSheetsTestPDF');
//cost center, course code & account api routes
Route::post('/store-course-code', 'CourseCodeController@store');
Route::put('/update-course-code/{id}', 'CourseCodeController@update');
Route::delete('/delete-course-code/{id}', 'CourseCodeController@delete');
Route::post('/store-cost-center', 'CostCenterController@store');
Route::put('/update-cost-center/{id}', 'CostCenterController@update');
Route::delete('/delete-cost-center/{id}', 'CostCenterController@delete');
Route::put('/update-account-details', 'AccountController@update');

//timerecord api routes
Route::post('/store-timerecord', 'TimeRecordController@store');
Route::put('/update-timerecord/{id}', 'TimeRecordController@update');
Route::delete('/delete-timerecord/{id}', 'TimeRecordController@delete');

//timesheet api routes
Route::post('/store-timesheet', 'TimeSheetController@store');
Route::put('/update-timesheet/{id}', 'TimeSheetController@update');
Route::delete('/delete-timesheet/{id}', 'TimeSheetController@delete');