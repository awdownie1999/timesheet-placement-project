<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('account', 'AccountController');
    Route::resource('costCenter', 'CostCenterController');
    Route::resource('courseCode', 'CourseCodeController');
    Route::resource('timeSheet', 'TimeSheetController');
    Route::resource('timeRecords', 'TimeRecordController');
    Route::post('/search', 'TimeSheetController@index')->name('search');
});

Route::get('/timeSheet/downloadts/{costCenterid?}/{innovationID?}/{colnnovateid?}/{ktpid?}/{academyid?}/{ptlid?}', 'TimeSheetController@generateTimeSheetsPDF')->name('timeSheet.generateTimeSheetsPDF');
Route::get('/timeSheet/{id}/downloadtimesheet/{costCenterid?}', 'TimeSheetController@generateTimeSheetPDF')->name('timeSheet.generateTimeSheetPDF');
Route::get('/timeSheet/{id}/downloadktp/{costCenterid?}', 'TimeSheetController@generateKtpCCPDF')->name('timeSheet.generateKtpPDF');
Route::get('/timeSheet/{id}/downloadacademy/{costCenterid?}', 'TimeSheetController@generateAcademyPDF')->name('timeSheet.generateAcademyPDF');
Route::get('/timeSheet/{id}/downloadcolnnovate/{costCenterid?}', 'TimeSheetController@generateColnnovatePDF')->name('timeSheet.generateColnnovatePDF');

Route::get('/timeSheet/{id}/downloadptl', 'TimeSheetController@generatePTLPDF')->name('timeSheet.generatePTLPDF');

Route::get('/tests', 'TimeSheetController@andrew')->name('timeSheet.andrew');
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/pdfdwl', 'TimeSheetController@generateTimeSheetsTestPDF')->name('timeSheet.generateTimeSheetsTestPDF');

Route::get('/test', 'VisitsController@getVisitsByMonth')->name('visits.test');
