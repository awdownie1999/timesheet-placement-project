@extends('layouts.app')

@section('content')

<div class="row">
    {{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Test Page</h1>
@php
    $month = "June";
@endphp
    <div>
        <a style="margin:19px;" href="{{route('visits.test')}}" class="btn btn-primary">Test No Param</a>
        <a style="margin:19px;" href="{{route('visits.test', $month)}}" class="btn btn-primary">Test Param</a>
    </div>

    <div>
        <h1>
            {{$monthVisits}}
        </h1>
    </div>
        {{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection
