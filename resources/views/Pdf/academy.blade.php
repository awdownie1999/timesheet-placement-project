<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <style>

        *{
            font-size: 14px;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            border-collapse: collapse;
        }
        
        #details{
            font-weight: 600;
            font-size: 13px;
            padding:left:500px;
        }
        .details-cells{
            text-align: center;
            padding-left: 150px;
            width: 150px;
        }

        #timesheet{
            border: 3px solid black;
        }
        
        .total{
            text-align: right;
            font-weight:700;
        }

        .headings{
            background-color: gray;
            border: 1px solid black;
        }
        
        .data{
            width: 180px;
            height: 10px;
            border: 1px solid black;
        }

        #employee-signature{
            border: 1px solid black;
        }

        #employee-signature td{
            border: 1px solid black;
        }

        #manager-signature{
            border: 1px solid black;
        }
        
        #manager-signature td{
            border: 1px solid black;
        }

        .date{
            width:180px;
        }

    </style>
</head>
<body>
    <img src="storage/app/public/Media/Belfast-Met-Logo.jpg" alt="logo">
    <table id="details">
        <tbody>
            <tr>
                <td class="details-cells">Course - </td>
            </tr>
            <tr>
                <td class="details-cells">PTL Name:</td>
            </tr>
            <tr>
            @if(isset($costCenter))
                <td class="details-cells">Cost Centre Code: {{ $costCenter->number }}</td>
            @else
                <td>Cost Centre Code:</td>
            @endif
            </tr>
            <tr>
                {{-- <td class="details-cells">Course Code: {{ $user->courseCode->number}}</td> --}}
            </tr>
        </tbody>
    </table>

    <table id="timesheet">
        <thead>
            <th class="headings">Date</th>
            <th class="headings" style="text-align: left">Subject</th>
            <th class="headings" style="padding: 0px 15px 0px 15px">Number of hours</th>
        </thead>
        
        <tbody>
            <tr></tr>
            @for($rows=1; $rows<10; $rows++)
                <tr>  
                    <td class="data"> </td>
                    <td class="data"> </td>
                    <td class="data"> </td>
                </tr>
            @endfor
            
            <tr>  
                <td class="data total"></td>
                <td class="data total">Total</td>
                <td class="data total">-</td>
            </tr>
        </tbody>
    </table>
    <br/>

    <table id="employee-signature">
        <tbody>
            <tr>
                <td class="data">Signed</td>
                <td class="data"></td>
            </tr>
            <tr>
                <td class="date">Date</td>
                <td class="date"> {{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}</td>
            </tr>
        </tbody>
    </table>
<br/>

    <table id="manager-signature">
        <tbody>
            <tr>
                <td class="data">Manager</td>
                <td class="data"> </td>
            </tr>
            <tr>
                <td class="date">Date</td>
                <td class="date"> </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
