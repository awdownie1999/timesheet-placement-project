<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <style>
        
        html,
        body {
            margin: auto;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }

        .user{
            border: 1px solid black;
            text-align: center;
            align:center;
            margin:auto;
            border-collapse: collapse;
        }
        .details{
            border:1px solid black;
            text-align: center;
            align:center;
            border-collapse: collapse;
            margin:auto;
            width: 700px;
        }

        .user td{
            border:1px solid black;
            border-collapse: collapse;
            font-weight: bold;
            text-align: left;
        }

        .details td{
            border:1px solid black;
            text-align: center;
            height: 12px;
        }

        .heading th{
            border:1px solid black;
            text-align: center;
        }
        
        #title {
            font-size: 18pt;
            text-align: center;
        }
        
        #subtitle {
            font-size: 18pt;
            text-align: center;
        }

        .colored_cells{
            font-size: 12pt;
            background-color: #E5DFEC;
            font-weight: bold;
            height: 15px
            text-align: center;
        }

        .colored_cells_2{
            font-size: 12pt;
            background-color: #C0C0C0;
            height: 12px;
        }

        .normal_cells{
            font-weight: 12pt;
        }

        .subtext{
            font-size: 10pt;
            background-color: #E5DFEC;
        }

        .signatures{
            margin-top: 20px;
            margin-left: 30px;
        }

        .activities{
            width: 300px;
        }

        .dates{
            width: 30px;
        }

        .hours{
            width: 80px;
        }
    </style>
</head>

<body>
    <h1 id="title"> KTP Programme</h1>
    <h1 id="subtitle">TIMESHEET</h1>

<table class="user">
    <tr>
        <td width="100" class="colored_cells">Name</td>
        <td width="350" class="normal_cells">{{ $user->name }}</td>
    </tr>
    <tr>
        <td width="100" rowspan="2" class="colored_cells">Month</td>
        <td width="350" rowspan="2" class="normal_cells">{{ $timeSheet->month }}</td>
    </tr>
    <tr></tr>
    <tr>
        <td width="100" class="colored_cells">Company</td>
        <td width="350" class="normal_cells">{{ $timeSheet->company }}</td>
    </tr>
    <tr>
        <td width="100" class="colored_cells">Cost Centre</td>
        @if(isset($costCenter))
            <td width="350" class="normal_cells">{{ $costCenter->number }}</td>
        @else
            <td width="350" class="normal_cells">All</td>
        @endif
    </tr>
</table>
<br/><br/>

<table class="details">
    <thead>
        <tr class="heading">
            <th rowspan="2" class="colored_cells dates">Date</th>
            <th class="colored_cells activities" style="border-collapse: collapse;">Activities</th>
            <th rowspan="2" class="colored_cells hours">Total Hours</th>
        </tr>
        <tr>
            <th class="subtext" style="border-collapse: collapse;">e.g.  application development, recruitment, mentoring</td>
        </tr>
    </thead>
    <tbody>
        @php
            $total = 0;
        @endphp
        @if($kTimeRecords != null)
            @foreach($kTimeRecords as $timeRecord)
                <tr>
                    <td class="colored_cells dates">{{ Carbon\Carbon::parse($timeRecord->dateOfRecord)->format('Y-m-d') }} </td>
                    <td class="colored_cells_2 activities">{{ $timeRecord->description }}</td>
                    <td class="colored_cells_2 hours">{{ $timeRecord->minutes/60 }}<</td>
                </tr>
                {{ $total += $timeRecord->minutes/60 }}
            @endforeach
        @endif

        <tr>
            <td width="100" rowspan="2" class="colored_cells">Total </td>
            <td width="250" rowspan="2" class="colored_cells_2"></td>
            <td width="100" rowspan="2" class="colored_cells_2">{{ $total }}</td>
        </tr>
        <tr></tr>
    </tbody>
    
</table>


<table class="signatures"><tr><td>Signed: __________
</td></tr>

<tr><td>Date: {{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}
</td></tr>

<tr><td>Verified by : __________
</td>
<td>Date: _________</td> 
</tr>
</table>





</body>

</html>