<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style>

        body {
            font-size: 10px;
            font-family:  Arial;
        }

        #timesheet {
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }

        #timesheet td, #customers th {
            border: 1px solid black;
            padding: 4px;
        }

        #timesheet th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

        .cell-right {
            text-align: right;
            font-weight: 700;
        }

        .description-header{
            font-weight: 700;
            text-align: center;
            background-color: gray;
        }

        .information {
            font-weight: 700;
            text-transform: uppercase;
        }

        .top-information{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }

        .time-sheet-title{
            text-align: center;
            font-weight: 700;
            font-size: 14px;
        }

        .declaration{
            font-weight: 700;
        }

        .bold-label {
            font-weight: 700;
        }

        #signature-table {
            border-collapse: collapse;
            width: 80%;
        }

        .underline-signature {
            border-bottom: 1px solid black;
            padding: 2px;
        }

        .total-for-month {
            text-align: right;
        }

        .center-number {
            text-align: center;
            height: 8px;
        }

    </style>
</head>
<body>

<p class="time-sheet-title">{{ $title }}</p>
<table class="top-information">
    <tbody>
    <tr>
        <td width="40" class="information">Name: </td>
        <td>{{ $user->name }}</td>
        <td width="80" class="information">Cost Center:</td>
        @if(isset($costCenter))
            <td>{{ $costCenter->number }}</td>
        @else
            <td></td>
        @endif
    </tr>
    <tr>
        <td width="40" class="information">Month:</td>
        <td>{{ $timeSheet->month }}</td>
        @if($decision == "timesheet")
            <td width="80" class="information">Company:</td>
            <td>{{ $timeSheet->company }}</td>
        @else
            <td width="80" class="information">Strand:</td>
            <td>{{ $timeSheet->strand }}</td>
        @endif
    </tr>
    </tbody>
</table>


<table id="timesheet">
    <thead>
    <tr class="description-header">
        <td colspan="2">Description of Activity</td>
        <td></td>
        <td colspan="2">Description of Activity</td>
        <td></td>
    </tr>
    </thead>

    @php 
        $subtotal = 0;
        $subtotal2 = 0;

    @endphp
    <tbody>
    @for($day=1; $day<=16; $day++)
    
        <tr>
            <td width="20" class="center-number">{{$day}}</td>
            @if(array_key_exists($timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . '0' . $day, $timeRecordsArray))
                <td width="180">aaa{{ $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . '0' . $day]->description }}</td> 
                <td width="20" class="center-number">{{ $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . '0' . $day]->minutes/60 }}</td>
                {{  $subtotal += $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . '0' . $day]->minutes/60 }}
                
            @elseif(array_key_exists($timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . $day, $timeRecordsArray))
                <td width="180">ssss{{ $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . $day]->description }}</td>
                <td width="20" class="center-number">{{ $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . $day]->minutes/60 }}</td>
                {{  $subtotal += $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . $day]->minutes/60 }}
            @else
                
                <td style="word-wrap:break-word;" width="180"></td>
                <td width="20" class="center-number"></td>
            @endif

            @if($day + 16 != 32)
                <td width="20" class="center-number">{{($day + 16) }}</td>
                @if(array_key_exists($timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . ($day + 16), $timeRecordsArray))
                    <td width="180">{{ $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . ($day + 16)]->description }}</td>
                    <td width="20" class="center-number">{{ $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . ($day + 16)]->minutes/60 }}</td>
                    {{  $subtotal2 += $timeRecordsArray[$timeSheet->year . '-' . '0' . Carbon\Carbon::parse($timeSheet->month)->month . '-' . ($day + 16)]->minutes/60 }}
                @else
                    <td style="word-wrap:break-word;" width="180"></td>
                    <td width="20" class="center-number"></td>
                @endif
                
            @else
                <td width="20" class="center-number"></td>
                <td style="word-wrap:break-word;" width="180"></td>
                <td width="20" class="center-number"></td>
            @endif
        </tr>
    @endfor

    <tr>
        <td class="cell-right" colspan="2">Subtotal</td>
        <td class="center-number">{{ $subtotal }}</td>
        <td class="cell-right"  colspan="2">Subtotal</td>
        <td class="center-number">{{ $subtotal2 }}</td>
    </tr>
    <tr>
        <td colspan="5" class="total-for-month"><b>TOTAL FOR MONTH</b></td>
        <td class="center-number">{{ $subtotal + $subtotal2}}</td>
    </tr>
    </tbody>
</table>

<p class="declaration">I declare that the details given on this form are true to the best of my knowledge</p>
<table id="signature-table">
    <tbody>
    <tr>
        <td width="40" class="bold-label">Signed:</td>
        <td class="underline-signature"></td>
        <td width="40" class="bold-label">Date:</td>
        <td class="underline-signature">{{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}}</td>
    </tr>
    <tr>
        <td width="40"></td>
        <td colspan="3">(Staff Member)</td>
    </tr>
    <tr>
        <td width="40"></td>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td width="40" class="bold-label">Signed:</td>
        <td class="underline-signature" ></td></td>
        <td width="40" class="bold-label">Date:</td>
        <td class="underline-signature" >{{ Carbon\Carbon::parse(Carbon\Carbon::now()->format('Y-m-d')) }}</td>
    </tr>
    <tr>
        <td width="40"></td>
        <td colspan="3">(Line Manager)</td>
    </tr>
    </tbody>
</table>
</body>
</html>
