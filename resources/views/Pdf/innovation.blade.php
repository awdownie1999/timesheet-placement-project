<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <style>

        html, body {
            width: 210mm;
            height: 297mm;
            size: A4;
            margin: 0;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }


        #title{
            font-weight: bold;
            text-align: center
            padding-left: 200px;
        }

        .activity{
            margin: auto;
            border 1px solid black;
        }

        .activity th{
            border: 1px solid black;
            border-collapse: collapse;
        }

        .activity td{
            border: 1px solid black;
            border-collapse: collapse; 
        }

        .leftDetails{
            text-align: left;
            padding-right: 200px;
        }

        .rightDetails{
            text-align: right;
            padding-left: 800px;
        }
    </style>
</head>

<body>
    <table class="head">
        <tbody>
            <tr>
                <td id="title">Innovation Voucher Timesheet</td>
                <td></td>
            </tr>
            <tr>
                <td class=leftDetails"><strong>NAME:</strong>  Andrew </td>
                @if(isset($costCenter))
                    <td class=rightDetails"><strong>COST CENTRE: {{ $costCenter->number }}</strong></td>
                @else
                    <td class=rightDetails"><strong>COST CENTRE: </strong></td>
                @endif
            </tr>
            <tr>
                <td class=leftDetails"><strong>MONTH:</strong> March</td>
                <td class=rightDetails"><strong>COMPANY: Mcdonalds</strong></td>
            </tr>
        </tbody>
    </table>
    <br/><br/>

<table class="activity">
    <tr>
        <th colspan="2">Description of Activity</th>
        <th></th>
        <th colspan="2">Description of Activity</th>
        <th></th>
    </tr>
    @for($i=1; $i<=16; $i++)
        <tr>
            <td width="60">{{ $i }}</td>
            <td width="180"></td>
            <td width="60"></td>
            @if($i + 16 != 32)
                <td width="60">{{  $i + 16}}</td>
            @else
                <td width="60"></td>
            @endif
            <td width="180"></td>
            <td width="60"></td>
        </tr>
    @endfor
    <tr>
        <td></td>
        <td class="subtotal">Subtotal</td>
        <td>14</td>
        <td></td>
        <td class="subtotal">Subtotal</td>
        <td>35</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="bottom_text">Total for Month</td>
        <td>49</td>
    </tr>
</table>

<table>
<tr>
    <td>I declare that the details given on this form are true to the best of my knowledge</td>
</tr>

<tr>
    <td>Signed ________________</td>
    <td>Date: ________________</td>
</tr>

<tr>
    <td>Signed ________________</td>
    <td>Date: ________________</td>
</tr>
</table>


</body>

</html>