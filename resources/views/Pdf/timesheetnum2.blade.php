<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style>

        body {
            font-size: 12px;
            font-family:  Arial;
        }

        #timesheet {
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }

        #timesheet td, #customers th {
            border: 1px solid black;
            padding: 4px;
        }

        #timesheet th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

        .cell-right {
            text-align: right;
            font-weight: 700;
        }

        .description-header{
            font-weight: 700;
            text-align: center;
            background-color: gray;
        }

        .information {
            font-weight: 700;
            text-transform: uppercase;
        }

        .top-information{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }

        .time-sheet-title{
            text-align: center;
            font-weight: 700;
            font-size: 14px;
        }

        .declaration{
            font-weight: 700;
        }

        .bold-label {
            font-weight: 700;
        }

        #signature-table {
            border-collapse: collapse;
            width: 80%;
        }

        .underline-signature {
            border-bottom: 1px solid black;
            padding: 2px;
        }

        .total-for-month {
            text-align: right;
        }

        .center-number {
            text-align: center;
        }

    </style>
</head>
<body>
<p class="time-sheet-title">Andrew</p>
<table class="top-information">
    <tbody>
    <tr>
        <td width="40" class="information">Name: </td>
        <td>Andrew</td>
        <td width="80" class="information">Cost Center:</td>
        <td>6048</td>
    </tr>
    <tr>
        <td width="40" class="information">Month:</td>
        <td>March</td>
        <td width="80" class="information">Company:</td>
        <td>Ethel and Stand</td>
    </tr>
    </tbody>
</table>
<table id="timesheet">
    <thead>
    <tr class="description-header">
        <td colspan="2">Description of Activity</td>
        <td></td>
        <td colspan="2">Description of Activity</td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    @for($day = 0; $day<=16; $day++)
        <tr>
            <td width="20" class="center-number">{{ $day }}</td>
            <td width="180">What I did?</td>
            <td width="25" class="center-number">7</td>
            @if($day + 16 != 32)
                <td width="20" class="center-number">{{ $day + 16 }}</td>
                <td style="word-wrap:break-word;" width="180">What I did here?adfasdfasdfasdfasfdsafasdfasdffffsasdffffffffasdfasdfasfasdfasdfddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</td>
                <td width="25" class="center-number">7</td>
            @else
                <td width="20" class="center-number"></td>
                <td style="word-wrap:break-word;" width="180"></td>
                <td width="25" class="center-number"></td>
            @endif
        </tr>
    @endfor
    <tr>
        <td class="cell-right" colspan="2">Subtotal</td>
        <td class="center-number">49</td>
        <td class="cell-right"  colspan="2">Subtotal</td>
        <td class="center-number">49</td>
    </tr>
    <tr>
        <td colspan="5" class="total-for-month"><b>TOTAL FOR MONTH</b></td>
        <td class="center-number">49</td>
    </tr>
    </tbody>
</table>
<p class="declaration">I declare that the details given on this form are true to the best of my knowledge</p>
<table id="signature-table">
    <tbody>
    <tr>
        <td width="40" class="bold-label">Signed:</td>
        <td class="underline-signature">Martin</td>
        <td width="40" class="bold-label">Date:</td>
        <td class="underline-signature">01/01/2020</td>
    </tr>
    <tr>
        <td width="40"></td>
        <td colspan="3">(Staff Member)</td>
    </tr>
    <tr>
        <td width="40"></td>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td width="40" class="bold-label">Signed:</td>
        <td class="underline-signature" >martin</td>
        <td width="40" class="bold-label">Date:</td>
        <td class="underline-signature" >01/01/2020</td>
    </tr>
    <tr>
        <td width="40"></td>
        <td colspan="3">(Line Manager)</td>
    </tr>
    </tbody>
</table>
</body>
</html>
