<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style>
        /*  Timesheet  */ 
        body {
            font-size: 10px;
            font-family:  Arial;
        }

        #timesheetT {
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }

        #timesheetT td, #customers th {
            border: 1px solid black;
            padding: 4px;
        }

        #timesheetT th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

        .cell-right {
            text-align: right;
            font-weight: 700;
        }

        .description-header{
            font-weight: 700;
            text-align: center;
            background-color: gray;
        }

        .information {
            font-weight: 700;
            text-transform: uppercase;
        }

        .top-information{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }

        .time-sheet-title{
            text-align: center;
            font-weight: 700;
            font-size: 14px;
        }

        .declaration{
            font-weight: 700;
        }

        .bold-label {
            font-weight: 700;
        }

        #signature-table {
            border-collapse: collapse;
            width: 80%;
        }

        .underline-signature {
            border-bottom: 1px solid black;
            padding: 2px;
        }

        .total-for-month {
            text-align: right;
        }
        .center-number {
            text-align: center;
            height: 8px;
        }



        /*  KTP  */ 
        .user{
            border: 1px solid black;
            text-align: center;
            align:center;
            margin:auto;
            border-collapse: collapse;
        }
        .details{
            border:1px solid black;
            text-align: center;
            align:center;
            border-collapse: collapse;
            margin:auto;
            width: 700px;
        }

        .user td{
            border:1px solid black;
            border-collapse: collapse;
            font-weight: bold;
            text-align: left;
        }

        .details td{
            border:1px solid black;
            text-align: center;
            height: 12px;
        }

        .heading th{
            border:1px solid black;
            text-align: center;
        }
        
        #titleK {
            font-size: 18pt;
            text-align: center;
        }
        
        #subtitleK {
            font-size: 18pt;
            text-align: center;
        }

        .colored_cells{
            font-size: 12pt;
            background-color: #E5DFEC;
            font-weight: bold;
            height: 15px
            text-align: center;
        }

        .colored_cells_2{
            font-size: 12pt;
            background-color: #C0C0C0;
            height: 12px;
        }

        .normal_cells{
            font-weight: 12pt;
        }

        .subtext{
            font-size: 10pt;
            background-color: #E5DFEC;
        }

        .signatures{
            margin-top: 20px;
            margin-left: 30px;
        }

        .activities{
            width: 300px;
        }

        .dates{
            width: 30px;
        }

        .hoursK{
            width: 80px;
        }



        /*  Academy  */
        #detailsA{
            font-weight: 600;
            font-size: 13px;
            padding:left:500px;
        }
        .details-cells{
            text-align: center;
            padding-left: 150px;
            width: 150px;
        }

        #timesheetA{
            border: 3px solid black;
        }
        
        .total{
            text-align: right;
            font-weight:700;
        }

        .headingsA{
            background-color: gray;
            border: 1px solid black;
        }
        
        .data{
            width: 180px;
            height: 10px;
            border: 1px solid black;
        }

        #employee-signature{
            border: 1px solid black;
        }

        #employee-signature td{
            border: 1px solid black;
        }

        #manager-signature{
            border: 1px solid black;
        }
        
        #manager-signature td{
            border: 1px solid black;
        }

        .date{
            width:180px;
        }


        /*  PTL  */

        .title {
            font-size: 19px;
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
        }

        .subtitle {
            font-size: 15px;
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
            width: 550px;
        }

        .courseDetails {
            font-size: 12px;
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
        }

        .userFillable {
            font-family: Calibri, 'Gill Sans', 'Gill Sans MT', 'Trebuchet MS';
            font-size: 15px;
            text-align: left;
        }

        .personalDetails {
            font-family: 'Times New Roman', Times, serif;
            font-size: 12px;
            font-weight: bold;
        }

        table {
            text-align: center;
            border-collapse: collapse;
            font-family: Calibri, sans-serif;
        }

        .entry td {
            border-right:  1px solid black;
            border-left:  1px solid black;
            border-bottom: 1px solid black;
            font-size: 12px;
        }

        .entry-double td {
            border-right:  1px solid black;
            border-left:  1px solid black;
            border-bottom: 3px double black;
            font-size: 12px;
        }

        .day {
            font-weight: 700;
            font-size: 13px;
            width: 32px;
        }

        .course-ref {
            width: 90px;
        }

        .subject {
            width:180px;
        }

        .grade {
            width:54px;
        }

        .enrolled {
            width:48px;
            font-size: 11px;
        }

        .time {
            width: 50px;
        }

        .week {
            width: 35px;
        }

        .hours {
            width: 25px;
        }

        .blue {
            width: 25px;
            background-color: #8DB3E2;
        }

        .total-grade {
            width: 50px;
        }

        .asst {
            width: 48px;
        }

        .cost-code {
            width: 48px;
        }

        #timesheet{
            border: 1px solid black;
            font-size: 12px;
            font-weight: 400;
        }

        .total-cells{
            border: 1px solid black;
            width: 48px;
        }
        
        .totalHours-cells{
            border: 1px solid black;
            width: 800px;
            text-align: right;
        }

        .signatures {
            font-weight: bold;
            text-align: left;
            font-family: 'Times New Roman', Times, serif;
            font-size: 12px;
        }

        .headings{
            border: 1px solid black;
        }
        
        #hours-heading{
            width: 80px;
        }

        .blue-heading {
            background-color: #8DB3E2;
            width: 80px;
        }
        
        .time-sub-headings{
            border: 1px solid black;
            font-size: 10px;
            font-weight: 200;
            width: 50px;
        }

        .sub-headings{
            border: 1px solid black;
            font-size: 10px;
            font-weight: 200;
            width: 53px;
        }
    </style>
</head>

<body>
    @if($colnnovateid!=null)
        <p class="time-sheet-title">{{ $cTitle }}</p>
        <table class="top-information">
            <tbody>
            <tr>
                <td width="40" class="information">Name: </td>
                <td>{{ $user->name }}</td>
                <td width="80" class="information">Cost Center:</td>
                @if(isset($costCenter))
                    <td>{{ $costCenter->number }}</td>
                @else
                    <td></td>
                @endif
            </tr>
            <tr>
                <td width="40" class="information">Month:</td>
                <td>{{ $cTimeSheet->month }}</td>
                <td width="80" class="information">Strand:</td>
                <td>{{ $cTimeSheet->strand }}</td>
            </tr>
            </tbody>
        </table>


        <table id="timesheetT">
            <thead>
            <tr class="description-header">
                <td colspan="2">Description of Activity</td>
                <td></td>
                <td colspan="2">Description of Activity</td>
                <td></td>
            </tr>
            </thead>

            @php 
                $subtotal = 0;
                $subtotal2 = 0;

            @endphp
            <tbody>
            @for($day=1; $day<=16; $day++)
            
                <tr>
                    <td width="20" class="center-number">{{$day}}</td>
                    @if(array_key_exists($cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . '0' . $day, $timeRecordsArrayColnnovate))
                        <td width="180">aaa{{ $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . '0' . $day]->description }}</td> 
                        <td width="20" class="center-number">{{ $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . '0' . $day]->minutes/60 }}</td>
                        {{  $subtotal += $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . '0' . $day]->minutes/60 }}
                        
                    @elseif(array_key_exists($cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . $day, $timeRecordsArrayColnnovate))
                        <td width="180">ssss{{ $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . $day]->description }}</td>
                        <td width="20" class="center-number">{{ $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . $day]->minutes/60 }}</td>
                        {{  $subtotal += $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . $day]->minutes/60 }}
                    @else
                        
                        <td style="word-wrap:break-word;" width="180"></td>
                        <td width="20" class="center-number"></td>
                    @endif

                    @if($day + 16 != 32)
                        <td width="20" class="center-number">{{($day + 16) }}</td>
                        @if(array_key_exists($cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . ($day + 16), $timeRecordsArrayColnnovate))
                            <td width="180">{{ $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . ($day + 16)]->description }}</td>
                            <td width="20" class="center-number">{{ $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . ($day + 16)]->minutes/60 }}</td>
                            {{  $subtotal2 += $timeRecordsArrayColnnovate[$cTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($cTimeSheet->month)->month . '-' . ($day + 16)]->minutes/60 }}
                        @else
                            <td style="word-wrap:break-word;" width="180"></td>
                            <td width="20" class="center-number"></td>
                        @endif
                        
                    @else
                        <td width="20" class="center-number"></td>
                        <td style="word-wrap:break-word;" width="180"></td>
                        <td width="20" class="center-number"></td>
                    @endif
                </tr>
            @endfor

            <tr>
                <td class="cell-right" colspan="2">Subtotal</td>
                <td class="center-number">{{ $subtotal }}</td>
                <td class="cell-right"  colspan="2">Subtotal</td>
                <td class="center-number">{{ $subtotal2 }}</td>
            </tr>
            <tr>
                <td colspan="5" class="total-for-month"><b>TOTAL FOR MONTH</b></td>
                <td class="center-number">{{ $subtotal + $subtotal2}}</td>
            </tr>
            </tbody>
        </table>
            
        <p class="declaration">I declare that the details given on this form are true to the best of my knowledge</p>
        <table id="signature-table">
            <tbody>
            <tr>
                <td width="40" class="bold-label">Signed:</td>
                <td class="underline-signature"></td>
                <td width="40" class="bold-label">Date:</td>
                <td class="underline-signature">{{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}}</td>
            </tr>
            <tr>
                <td width="40"></td>
                <td colspan="3">(Staff Member)</td>
            </tr>
            <tr>
                <td width="40"></td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td width="40" class="bold-label">Signed:</td>
                <td class="underline-signature" ></td></td>
                <td width="40" class="bold-label">Date:</td>
                <td class="underline-signature" >{{ Carbon\Carbon::parse(Carbon\Carbon::now()->format('Y-m-d')) }}</td>
            </tr>
            <tr>
                <td width="40"></td>
                <td colspan="3">(Line Manager)</td>
            </tr>
            </tbody>
        </table>
    @endif
    <br/><br/><br/><br/><br/><br/>

    @if($innovationID != null)
    <p class="time-sheet-title">{{ $iTitle }}</p>
    <table class="top-information">
        <tbody>
        <tr>
            <td width="40" class="information">Name: </td>
            <td>{{ $user->name }}</td>
            <td width="80" class="information">Cost Center:</td>
            @if(isset($costCenter))
                <td>{{ $costCenter->number }}</td>
            @else
                <td></td>
            @endif
        </tr>
        <tr>
            <td width="40" class="information">Month:</td>
            <td>{{ $iTimeSheet->month }}</td>
            <td width="80" class="information">Company:</td>
            <td>{{ $iTimeSheet->company }}</td>
        </tr>
        </tbody>
    </table>


    <table id="timesheetT">
        <thead>
        <tr class="description-header">
            <td colspan="2">Description of Activity</td>
            <td></td>
            <td colspan="2">Description of Activity</td>
            <td></td>
        </tr>
        </thead>

        @php 
            $subtotal = 0;
            $subtotal2 = 0;

        @endphp
        <tbody>
        @for($day=1; $day<=16; $day++)
        
            <tr>
                <td width="20" class="center-number">{{$day}}</td>
                @if(array_key_exists($iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . '0' . $day, $timeRecordsArrayInnovation))
                    <td width="180">aaa{{ $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . '0' . $day]->description }}</td> 
                    <td width="20" class="center-number">{{ $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . '0' . $day]->minutes/60 }}</td>
                    {{  $subtotal += $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . '0' . $day]->minutes/60 }}
                    
                @elseif(array_key_exists($iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . $day, $timeRecordsArrayInnovation))
                    <td width="180">ssss{{ $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . $day]->description }}</td>
                    <td width="20" class="center-number">{{ $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . $day]->minutes/60 }}</td>
                    {{  $subtotal += $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . $day]->minutes/60 }}
                @else
                    
                    <td style="word-wrap:break-word;" width="180"></td>
                    <td width="20" class="center-number"></td>
                @endif

                @if($day + 16 != 32)
                    <td width="20" class="center-number">{{($day + 16) }}</td>
                    @if(array_key_exists($iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . ($day + 16), $timeRecordsArrayInnovation))
                        <td width="180">{{ $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . ($day + 16)]->description }}</td>
                        <td width="20" class="center-number">{{ $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . ($day + 16)]->minutes/60 }}</td>
                        {{  $subtotal2 += $timeRecordsArrayInnovation[$iTimeSheet->year . '-' . '0' . Carbon\Carbon::parse($iTimeSheet->month)->month . '-' . ($day + 16)]->minutes/60 }}
                    @else
                        <td style="word-wrap:break-word;" width="180"></td>
                        <td width="20" class="center-number"></td>
                    @endif
                    
                @else
                    <td width="20" class="center-number"></td>
                    <td style="word-wrap:break-word;" width="180"></td>
                    <td width="20" class="center-number"></td>
                @endif
            </tr>
        @endfor

        <tr>
            <td class="cell-right" colspan="2">Subtotal</td>
            <td class="center-number">{{ $subtotal }}</td>
            <td class="cell-right"  colspan="2">Subtotal</td>
            <td class="center-number">{{ $subtotal2 }}</td>
        </tr>
        <tr>
            <td colspan="5" class="total-for-month"><b>TOTAL FOR MONTH</b></td>
            <td class="center-number">{{ $subtotal + $subtotal2}}</td>
        </tr>
        </tbody>
    </table>

    <p class="declaration">I declare that the details given on this form are true to the best of my knowledge</p>
    <table id="signature-table">
        <tbody>
        <tr>
            <td width="40" class="bold-label">Signed:</td>
            <td class="underline-signature"></td>
            <td width="40" class="bold-label">Date:</td>
            <td class="underline-signature">{{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}}</td>
        </tr>
        <tr>
            <td width="40"></td>
            <td colspan="3">(Staff Member)</td>
        </tr>
        <tr>
            <td width="40"></td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td width="40" class="bold-label">Signed:</td>
            <td class="underline-signature" ></td></td>
            <td width="40" class="bold-label">Date:</td>
            <td class="underline-signature" >{{ Carbon\Carbon::parse(Carbon\Carbon::now()->format('Y-m-d')) }}</td>
        </tr>
        <tr>
            <td width="40"></td>
            <td colspan="3">(Line Manager)</td>
        </tr>
        </tbody>
    </table>
    @endif

    <br/><br/><br/><br/><br/><br/>

    @if($ktpid != null)
        <h1 id="titleK"> KTP Programme</h1>
        <h1 id="subtitleK">TIMESHEET</h1>

        <table class="user">
            <tr>
                <td width="100" class="colored_cells">Name</td>
                <td width="350" class="normal_cells">{{ $user->name }}</td>
            </tr>
            <tr>
                <td width="100" rowspan="2" class="colored_cells">Month</td>
                <td width="350" rowspan="2" class="normal_cells">{{ $kTimeSheet->month }}</td>
            </tr>
            <tr></tr>
            <tr>
                <td width="100" class="colored_cells">Company</td>
                <td width="350" class="normal_cells">{{ $kTimeSheet->company }}</td>
            </tr>
            <tr>
                <td width="100" class="colored_cells">Cost Centre</td>
                @if(isset($costCenter))
                    <td width="350" class="normal_cells">{{ $costCenter->number }}</td>
                @else
                    <td width="350" class="normal_cells">All</td>
                @endif
            </tr>
        </table>
        <br/><br/>

        <table class="details">
            <thead>
                <tr class="heading">
                    <th rowspan="2" class="colored_cells dates">Date</th>
                    <th class="colored_cells activities" style="border-collapse: collapse;">Activities</th>
                    <th rowspan="2" class="colored_cells hoursK">Total Hours</th>
                </tr>
                <tr>
                    <th class="subtext" style="border-collapse: collapse;">e.g.  application development, recruitment, mentoring</td>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                @endphp
                @if($kTimeRecords != null)
                    @foreach($kTimeRecords as $timeRecord)
                        <tr>
                            <td class="colored_cells dates">{{ Carbon\Carbon::parse($timeRecord->dateOfRecord)->format('Y-m-d') }} </td>
                            <td class="colored_cells_2 activities">{{ $timeRecord->description }}</td>
                            <td class="colored_cells_2 hours">{{ $timeRecord->minutes/60 }}<</td>
                        </tr>
                        {{ $total += $timeRecord->minutes/60 }}
                    @endforeach
                @endif
                <tr>
                    <td width="100" rowspan="2" class="colored_cells">Total </td>
                    <td width="250" rowspan="2" class="colored_cells_2"></td>
                    <td width="100" rowspan="2" class="colored_cells_2">{{ $total }}</td>
                </tr>
                <tr></tr>
            </tbody>
        </table>

        <table class="signatures">
            <tr>
                <td>Signed: __________</td>
            </tr>
            <tr>
                <td>Date: {{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}</td>
            </tr>
            <tr>
                <td>Verified by : __________</td>
                <td>Date: _________</td> 
            </tr>
        </table>
    @endif

    <br/><br/><br/><br/><br/><br/>

    @if($academyid != null)
        <img src="storage/app/public/Media/Belfast-Met-Logo.jpg" alt="logo">
        <table id="detailsA">
            <tbody>
                <tr>
                    <td class="details-cells">Course - </td>
                </tr>
                <tr>
                    <td class="details-cells">PTL Name:</td>
                </tr>
                <tr>
                @if(isset($costCenter))
                    <td class="details-cells">Cost Centre Code: {{ $costCenter->number }}</td>
                @else
                    <td>Cost Centre Code:</td>
                @endif
                </tr>
                <tr>
                    {{-- <td class="details-cells">Course Code: {{ $user->courseCode->number}}</td> --}}
                </tr>
            </tbody>
        </table>

        <table id="timesheetA">
            <thead>
                <th class="headingsA">Date</th>
                <th class="headingsA" style="text-align: left">Subject</th>
                <th class="headingsA" style="padding: 0px 15px 0px 15px">Number of hours</th>
            </thead>
            <tbody>
                <tr></tr>
                @for($rows=1; $rows<10; $rows++)
                    <tr>  
                        <td class="data"> </td>
                        <td class="data"> </td>
                        <td class="data"> </td>
                    </tr>
                @endfor
                <tr>  
                    <td class="data total"></td>
                    <td class="data total">Total</td>
                    <td class="data total">-</td>
                </tr>
            </tbody>
        </table>
        <br/>
        <table id="employee-signature">
            <tbody>
                <tr>
                    <td class="data">Signed</td>
                    <td class="data"></td>
                </tr>
                <tr>
                    <td class="date">Date</td>
                    <td class="date"> {{ Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}</td>
                </tr>
            </tbody>
        </table>
        <br/>
        <table id="manager-signature">
            <tbody>
                <tr>
                    <td class="data">Manager</td>
                    <td class="data"> </td>
                </tr>
                <tr>
                    <td class="date">Date</td>
                    <td class="date"> </td>
                </tr>
            </tbody>
        </table>
    @endif

    <br/><br/><br/><br/><br/><br/>

    @if($ptlid != null)
        <table>
            <tbody>
                <tr class="title">
                    <td style="padding-right: 360px;">Centre For __________</td>
                    <td style="padding-left: 400px; padding-right: 50px;">PAY1</td>
                </tr>
                <tr>
                    <td class="subtitle">Incorporating (please indicate below the Cost Centres to which this claim relates)</td>
                    <td class="courseDetails" style="padding-left: 150px;">Is this your first claim? No (delete as appropriate) **</td>
                </tr>

            </tbody>
        </table>

        <table>
            <tbody>
                <tr>
                    <td class="courseDetails">LIST COST CENTRES HERE IF (APPLICABLE) 5555 4444 3333</td>
                    {{-- <td class="userFillable" style="padding-left: 150px;">Course Code {{ $user->courseCode->number }} --}}
                    </td>
                </tr>
            </tbody>
        </table>

        <table>
            <tbody>
                <tr>
                    <td class="courseDetails">PART –TIME LECTURER’S SALARY CLAIM FOR MONTH OF {{ $ptlTimeSheet->month }}</td>
                    <td class="userFillable" style="padding-left: 100px;">Year <strong>{{ $ptlTimeSheet->year }}</strong></td>
                </tr>

            </tbody>
        </table>

        <table>
            <tbody>
                <tr>
                    <td class="courseDetails" style="width: 800px;">NB PLEASE REFER TO THE NOTES OVERLEAF BEFORE COMPLETING THIS FORM (complete all parts).</td>
                </tr>
            </tbody>
        </table>
        <table>
            <tbody>
                <tr>
                    <td class="personalDetails">Title_____</td>
                    <td class="personalDetails" style="padding-left: 80px;">Forename(s);{{ $user->name }}</td>
                    <td class="personalDetails" style="padding-left: 80px;">Surname_____</td>
                    @if(isset($user->employee_number))
                        <td class="personalDetails" style="padding-left: 80px;">Staff No.{{ $user->employee_number }}</td>
                    @else
                        <td class="personalDetails" style="padding-left: 80px;">Staff No. _______</td>
                    @endif
                
                </tr>
            </tbody>
        </table>
        <table id="timesheet">
            <thead>
                <tr>
                    <th rowspan="2" class="day headings">Day</th>
                    <th rowspan="2" class="course-ref headings">Course REF</th>
                    <th rowspan="2" class="subject headings">Subject</th>
                    <th rowspan="2" class="grade headings">Class Grade (5, 4 or 3)</th>
                    <th rowspan="2" class="enrolled headings">No enrolled in class</th>
                    <th colspan="2" class="time headings">Time</th>
                    <th colspan="5" class="headings" id="hours-heading">No of hours worked</th>
                    <th colspan="5" class="blue-heading">No of students present</th>
                    <th rowspan="2" class="headings total-grade">Total Grade 3 Hours</th>
                    <th rowspan="2" class="headings total-grade">Total Grade 4 Hours</th>
                    <th rowspan="2" class="headings total-grade">Total Grade 5 Hours</th>
                    <th rowspan="2" class="headings asst">Asst.</th>
                    <th rowspan="2" class="headings cost-code">Cost Code</th>
                </tr>
                <tr>
                    <th class="time-sub-headings">Start</th>
                    <th class="time-sub-headings">Finish</th>
                    <th colspan="2" class="sub-headings">Week 1</th>
                    <th colspan="2" class="sub-headings">Week 2</th>
                    <th colspan="2" class="sub-headings">Week 3</th>
                    <th colspan="2" class="sub-headings">Week 4</th>
                    <th colspan="2" class="sub-headings">Week 5</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        @php
            $totalG3 =0;
            $totalG4 =0;
            $totalG5 =0;
        @endphp
        
        @for($day_number = 0; $day_number<= 5;$day_number++)
        <table style="page-break-inside:avoid;">
        <tbody>
        @if(count($days[$weekMap[($day_number+1)]]) == 0)

            <tr class="entry">  
            <td rowspan="{{ count($days[$weekMap[($day_number+1)]])}}" class="day" valign="bottom">{{ $weekMap[($day_number+1)] }}</td>
            <td class="course-ref">  </td> 
                        <td class="subject"> </td>
                        <td class="grade"> </td>
                        <td class="enrolled"> </td>
                        <td class="time"> </td>
                        <td class="time"> </td>
                        <td class="hours"> </td>
                        <td class="blue"> </td>
                        <td class="hours"> </td>
                        <td class="blue"> </td>
                        <td class="hours"> </td>
                        <td class="blue"> </td>
                        <td class="hours"> </td>
                        <td class="blue"> </td>
                        <td class="hours"> </td>
                        <td class="blue"> </td>
                        <td class="total-grade"> </td>
                        <td class="total-grade"> </td>
                        <td class="total-grade"> </td>
                        <td class="asst"> </td>
                        <td class="cost-code"> </td>
            </tr>

        @endif
        @for($day_entry = 0; $day_entry  < count($days[$weekMap[($day_number+1)]]); $day_entry++)
                    <tr class="{{ $day_entry == count($days[$weekMap[($day_number+1)]]) -1 ? 'entry-double' : 'entry'  }}">  
                        @if($day_entry == 0)
                            <td rowspan="{{ count($days[$weekMap[($day_number+1)]])}}" class="day" valign="bottom">{{ $weekMap[($day_number+1)] }}</td>
                        @endif
                    
                        <td class="course-ref">{{ $days[$weekMap[($day_number+1)]][$day_entry]['course_ref'] }}</td> 
                        <td class="subject">{{ $days[$weekMap[($day_number+1)]][$day_entry]['subject'] }}</td>
                        <td class="grade">{{ $days[$weekMap[($day_number+1)]][$day_entry]['grade'] }} </td>
                        <td class="enrolled"> </td>
                        <td class="time">{{ $days[$weekMap[($day_number+1)]][$day_entry]['start_time'] }}</td>
                        <td class="time">{{ $days[$weekMap[($day_number+1)]][$day_entry]['end_time'] }}</td>
                        
                        @php
                            $grade3Hours = 0;
                            $grade4Hours = 0;
                            $grade5Hours = 0;
                        @endphp
                        @for($weekIndex = 0; $weekIndex < count($days[$weekMap[($day_number+1)]][$day_entry]['week_hours']); $weekIndex++)
                        
                            @if($days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex] == 0)
                            <td class="hours"> </td>
                            @else
                            <td class="hours">{{ $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex] }}</td>
                            @endif

                            @if( $days[$weekMap[($day_number+1)]][$day_entry]['grade'] == 3)
                                {{  $grade3Hours += $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex]}}
                            @elseif( $days[$weekMap[($day_number+1)]][$day_entry]['grade'] == 4)
                                {{  $grade4Hours += $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex]}}
                            @elseif( $days[$weekMap[($day_number+1)]][$day_entry]['grade'] == 5)
                                {{  $grade5Hours += $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex]}}
                            @endif
                            <td class="blue">0</td>
                        @endfor
                        <td class="total-grade">{{ $grade3Hours }} </td> {{ $totalG3 += $grade3Hours }}
                        <td class="total-grade">{{ $grade4Hours }} </td> {{ $totalG4 += $grade4Hours }}
                        <td class="total-grade">{{ $grade5Hours }} </td> {{ $totalG5 += $grade5Hours }}
                    <td class="asst"> </td>
                    <td class="cost-code">{{ $days[$weekMap[($day_number+1)]][$day_entry]['cost_center'] }}</td>
                </tr>
            @endfor 
        </tbody>
        </table>
        @endfor

        <table>
            <tbody>
                <tr>
                    <td class="totalHours-cells" >Total Hours Worked</td>
                    <td class="total-cells">{{ $totalG3 }}</td>
                    <td class="total-cells">{{ $totalG4 }}</td>
                    <td class="total-cells">{{ $totalG5 }}</td>
                    <td class="total-cells"> </td>
                    <td class="total-cells"> </td>
                </tr>
            </tbody>
        </table>

        <table id="signature-table">
            <tbody>
                <tr class="signatures">
                    <td>I certify that the hours indicated were worked</td>
                </tr>
                <tr class="signatures">
                    <td>I certify that the hours indicated were requested</td>
                    <td style="padding-left: 25px;">Signature (Claimant): &nbsp;__________________________________________</td>
                    <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
                </tr>

                <tr class="signatures">
                    <td>I certify that a corresponding contract exists</td>
                    <td style="padding-left: 25px;">Signature (Curriculum Area Mgr):&nbsp; _______________________________</td>
                    <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
                </tr>

                <tr class="signatures">
                    <td>I certify that payment is to be authorised</td>
                    <td style="padding-left: 25px;">Signature (Administrator/Secretary):&nbsp; _____________________________</td>
                    <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
                </tr>
                <tr class="signatures">
                    <td></td>
                    <td style="padding-left: 25px;">&nbsp;Signature (Head of School):&nbsp; ______________________________________</td>
                    <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
                </tr>

            </tbody>
        </table>

    @endif
</body>
</html>
