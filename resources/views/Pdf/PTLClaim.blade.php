<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <style>
        body {
        }

        .title {
            font-size: 19px;
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
        }

        .subtitle {
            font-size: 15px;
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
            width: 550px;
        }

        .courseDetails {
            font-size: 12px;
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
        }

        .userFillable {
            font-family: Calibri, 'Gill Sans', 'Gill Sans MT', 'Trebuchet MS';
            font-size: 15px;
            text-align: left;
        }

        .personalDetails {
            font-family: 'Times New Roman', Times, serif;
            font-size: 12px;
            font-weight: bold;
        }

        table {
            text-align: center;
            border-collapse: collapse;
            font-family: Calibri, sans-serif;
        }

        .entry td {
            border-right:  1px solid black;
            border-left:  1px solid black;
            border-bottom: 1px solid black;
            font-size: 12px;
        }

        .entry-double td {
            border-right:  1px solid black;
            border-left:  1px solid black;
            border-bottom: 3px double black;
            font-size: 12px;
        }

        .day {
            font-weight: 700;
            font-size: 13px;
            width: 32px;
        }

        .course-ref {
            width: 90px;
        }

        .subject {
            width:180px;
        }

        .grade {
            width:54px;
        }

        .enrolled {
            width:48px;
            font-size: 11px;
        }

        .time {
            width: 50px;
        }

        .week {
            width: 35px;
        }

        .hours {
            width: 25px;
        }

        .blue {
            width: 25px;
            background-color: #8DB3E2;
        }

        .total-grade {
            width: 50px;
        }

        .asst {
            width: 48px;
        }

        .cost-code {
            width: 48px;
        }

        #timesheet{
            border: 1px solid black;
            font-size: 12px;
            font-weight: 400;
        }

        .total-cells{
            border: 1px solid black;
            width: 48px;
        }
        
        .totalHours-cells{
            border: 1px solid black;
            width: 800px;
            text-align: right;
        }

        .signatures {
            font-weight: bold;
            text-align: left;
            font-family: 'Times New Roman', Times, serif;
            font-size: 12px;
        }

        .headings{
            border: 1px solid black;
        }
        
        #hours-heading{
            width: 80px;
        }

        .blue-heading {
            background-color: #8DB3E2;
            width: 80px;
        }
        
        .time-sub-headings{
            border: 1px solid black;
            font-size: 10px;
            font-weight: 200;
            width: 50px;
        }

        .sub-headings{
            border: 1px solid black;
            font-size: 10px;
            font-weight: 200;
            width: 53px;
        }
    </style>
</head>

<body>
    <table>
        <tbody>
            <tr class="title">
                <td style="padding-right: 360px;">Centre For __________</td>
                <td style="padding-left: 400px; padding-right: 50px;">PAY1</td>
            </tr>
            <tr>
                <td class="subtitle">Incorporating (please indicate below the Cost Centres to which this claim relates)</td>
                <td class="courseDetails" style="padding-left: 150px;">Is this your first claim? No (delete as appropriate) **</td>
            </tr>

        </tbody>
    </table>

    <table>
        <tbody>
            <tr>
                <td class="courseDetails">LIST COST CENTRES HERE IF (APPLICABLE) 5555 4444 3333</td>
                {{-- <td class="userFillable" style="padding-left: 150px;">Course Code {{ $user->courseCode->number }} --}}
                </td>
            </tr>
        </tbody>
    </table>

    <table>
        <tbody>
            <tr>
                <td class="courseDetails">PART –TIME LECTURER’S SALARY CLAIM FOR MONTH OF {{ $timeSheet->month }}</td>
                <td class="userFillable" style="padding-left: 100px;">Year <strong>{{ $timeSheet->year }}</strong></td>
            </tr>

        </tbody>
    </table>

    <table>
        <tbody>
            <tr>
                <td class="courseDetails" style="width: 800px;">NB PLEASE REFER TO THE NOTES OVERLEAF BEFORE COMPLETING THIS FORM (complete all parts).</td>
            </tr>
        </tbody>
    </table>
    <table>
        <tbody>
            <tr>
                <td class="personalDetails">Title_____</td>
                <td class="personalDetails" style="padding-left: 80px;">Forename(s);{{ $user->name }}</td>
                <td class="personalDetails" style="padding-left: 80px;">Surname_____</td>
                @if(isset($user->employee_number))
                    <td class="personalDetails" style="padding-left: 80px;">Staff No.{{ $user->employee_number }}</td>
                @else
                    <td class="personalDetails" style="padding-left: 80px;">Staff No. _______</td>
                @endif
            
            </tr>
        </tbody>
    </table>
    <table id="timesheet">
        <thead>
            <tr>
                <th rowspan="2" class="day headings">Day</th>
                <th rowspan="2" class="course-ref headings">Course REF</th>
                <th rowspan="2" class="subject headings">Subject</th>
                <th rowspan="2" class="grade headings">Class Grade (5, 4 or 3)</th>
                <th rowspan="2" class="enrolled headings">No enrolled in class</th>
                <th colspan="2" class="time headings">Time</th>
                <th colspan="5" class="headings" id="hours-heading">No of hours worked</th>
                <th colspan="5" class="blue-heading">No of students present</th>
                <th rowspan="2" class="headings total-grade">Total Grade 3 Hours</th>
                <th rowspan="2" class="headings total-grade">Total Grade 4 Hours</th>
                <th rowspan="2" class="headings total-grade">Total Grade 5 Hours</th>
                <th rowspan="2" class="headings asst">Asst.</th>
                <th rowspan="2" class="headings cost-code">Cost Code</th>
            </tr>
              <tr>
                <th class="time-sub-headings">Start</th>
                <th class="time-sub-headings">Finish</th>
                <th colspan="2" class="sub-headings">Week 1</th>
                <th colspan="2" class="sub-headings">Week 2</th>
                <th colspan="2" class="sub-headings">Week 3</th>
                <th colspan="2" class="sub-headings">Week 4</th>
                <th colspan="2" class="sub-headings">Week 5</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    @php
        $totalG3 =0;
        $totalG4 =0;
        $totalG5 =0;
    @endphp
    
    @for($day_number = 0; $day_number<= 5;$day_number++)
    <table style="page-break-inside:avoid;">
    <tbody>
    @if(count($days[$weekMap[($day_number+1)]]) == 0)

        <tr class="entry">  
        <td rowspan="{{ count($days[$weekMap[($day_number+1)]])}}" class="day" valign="bottom">{{ $weekMap[($day_number+1)] }}</td>
        <td class="course-ref">  </td> 
                    <td class="subject"> </td>
                    <td class="grade"> </td>
                    <td class="enrolled"> </td>
                    <td class="time"> </td>
                    <td class="time"> </td>
                    <td class="hours"> </td>
                    <td class="blue"> </td>
                    <td class="hours"> </td>
                    <td class="blue"> </td>
                    <td class="hours"> </td>
                    <td class="blue"> </td>
                    <td class="hours"> </td>
                    <td class="blue"> </td>
                    <td class="hours"> </td>
                    <td class="blue"> </td>
                    <td class="total-grade"> </td>
                    <td class="total-grade"> </td>
                    <td class="total-grade"> </td>
                    <td class="asst"> </td>
                    <td class="cost-code"> </td>
        </tr>

    @endif
      @for($day_entry = 0; $day_entry  < count($days[$weekMap[($day_number+1)]]); $day_entry++)
                <tr class="{{ $day_entry == count($days[$weekMap[($day_number+1)]]) -1 ? 'entry-double' : 'entry'  }}">  
                    @if($day_entry == 0)
                        <td rowspan="{{ count($days[$weekMap[($day_number+1)]])}}" class="day" valign="bottom">{{ $weekMap[($day_number+1)] }}</td>
                    @endif
                
                    <td class="course-ref">{{ $days[$weekMap[($day_number+1)]][$day_entry]['course_ref'] }}</td> 
                    <td class="subject">{{ $days[$weekMap[($day_number+1)]][$day_entry]['subject'] }}</td>
                    <td class="grade">{{ $days[$weekMap[($day_number+1)]][$day_entry]['grade'] }} </td>
                    <td class="enrolled"> </td>
                    <td class="time">{{ $days[$weekMap[($day_number+1)]][$day_entry]['start_time'] }}</td>
                    <td class="time">{{ $days[$weekMap[($day_number+1)]][$day_entry]['end_time'] }}</td>
                    
                    @php
                        $grade3Hours = 0;
                        $grade4Hours = 0;
                        $grade5Hours = 0;
                    @endphp
                    @for($weekIndex = 0; $weekIndex < count($days[$weekMap[($day_number+1)]][$day_entry]['week_hours']); $weekIndex++)
                    
                        @if($days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex] == 0)
                        <td class="hours"> </td>
                        @else
                        <td class="hours">{{ $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex] }}</td>
                        @endif

                        @if( $days[$weekMap[($day_number+1)]][$day_entry]['grade'] == 3)
                            {{  $grade3Hours += $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex]}}
                        @elseif( $days[$weekMap[($day_number+1)]][$day_entry]['grade'] == 4)
                            {{  $grade4Hours += $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex]}}
                        @elseif( $days[$weekMap[($day_number+1)]][$day_entry]['grade'] == 5)
                            {{  $grade5Hours += $days[$weekMap[($day_number+1)]][$day_entry]['week_hours'][$weekIndex]}}
                        @endif
                        <td class="blue">0</td>
                    @endfor
                    <td class="total-grade">{{ $grade3Hours }} </td> {{ $totalG3 += $grade3Hours }}
                    <td class="total-grade">{{ $grade4Hours }} </td> {{ $totalG4 += $grade4Hours }}
                    <td class="total-grade">{{ $grade5Hours }} </td> {{ $totalG5 += $grade5Hours }}
                <td class="asst"> </td>
                <td class="cost-code">{{ $days[$weekMap[($day_number+1)]][$day_entry]['cost_center'] }}</td>
            </tr>
        @endfor 
    </tbody>
    </table>
    @endfor

    <table>
        <tbody>
            <tr>
                <td class="totalHours-cells" >Total Hours Worked</td>
                <td class="total-cells">{{ $totalG3 }}</td>
                <td class="total-cells">{{ $totalG4 }}</td>
                <td class="total-cells">{{ $totalG5 }}</td>
                <td class="total-cells"> </td>
                <td class="total-cells"> </td>
            </tr>
        </tbody>
    </table>

    <table id="signature-table">
        <tbody>
            <tr class="signatures">
                <td>I certify that the hours indicated were worked</td>
            </tr>
            <tr class="signatures">
                <td>I certify that the hours indicated were requested</td>
                <td style="padding-left: 25px;">Signature (Claimant): &nbsp;__________________________________________</td>
                <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
            </tr>

            <tr class="signatures">
                <td>I certify that a corresponding contract exists</td>
                <td style="padding-left: 25px;">Signature (Curriculum Area Mgr):&nbsp; _______________________________</td>
                <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
            </tr>

            <tr class="signatures">
                <td>I certify that payment is to be authorised</td>
                <td style="padding-left: 25px;">Signature (Administrator/Secretary):&nbsp; _____________________________</td>
                <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
            </tr>
            <tr class="signatures">
                <td></td>
                <td style="padding-left: 25px;">&nbsp;Signature (Head of School):&nbsp; ______________________________________</td>
                <td>&nbsp;&nbsp;&nbsp;Date ____/_____/_____</td>
            </tr>

        </tbody>
    </table>
</body>

</html>
