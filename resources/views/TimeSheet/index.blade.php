@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Timesheets</h1>
        <div>
	{{-- Error Checking --}}
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
    {{-- <select name="pdfFormat">
        <option value="ktp">ktp</option>
        <option value="timesheet">timesheet</option>
    </select> --}}
    {{-- <label>CostCenter ID</label>
    <select id="costCenter" name="costCenter">
        <option value="">Please Select</option>
        @foreach($costCenters as $costCenter)
            <option value="{{ $costCenter->id }}">{{ $costCenter->number }}</option>
        @endforeach
    </select> --}}




    {{-- template stored as numbers access this, query string sent over --}}
     {{-- @php
         
        // if(isset($_POST['costCenterId'])) {
        //     $ccSelected = $_POST['costCenterId'];
        // }else{
        //     $ccSelected = 0;
        // }
    @endphp --}}

    @if($emptySearch == 0)
            <h2>No Search Results</h2>
    @endif
    <div>
        <form class="form-inline" action="/search" method="POST" role="search" >
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" name="q" width="500px"
                    placeholder="Search Timesheets by Name"> <span class="input-group-btn">&nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-primary">Search</button>
                </span>
            </div>
        </form>
        <a style="margin:19px;" href="{{route('timeSheet.create')}}" class="btn btn-primary">New Time Sheet</a>
        <a style="margin:19px;" href="{{route('timeSheet.index', ['filter'=> 'all'])}}" class="btn btn-primary">Show All</a>
    </div>
        <table>
            <thead>
                <tr>
                    <th>Name&nbsp;&nbsp;</th>
                    <th>Month&nbsp;&nbsp;</th>
                    <th>Year&nbsp;&nbsp;</th>
                    <th>Total Hours</th>
                    <th>Total Hours Grade 3</th>
                    <th>Total Hours Grade 4</th>
                    <th>Total Hours Grade 5</th>
                </tr>
            </thead>
            @php
                $costCenterid = 1;
                // $pdfArr = array("colnnovate"=>true, "ktp"=>true, "academy"=>false, "ptl"=>true, "innovation"=>false);
            @endphp
            <tbody>
                @foreach ($timeSheets as $timeSheet)
                    <tr>
                        <td>{{ $timeSheet->name }}&nbsp;&nbsp;</td>
                        <td>{{ $timeSheet->month }}&nbsp;&nbsp;</td>
                        <td>{{ $timeSheet->year  }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>{{ $timeSheet->total_minutes/60  }}</td>
                        <td>{{ $timeSheet->total_minutes_grade_3/60  }}</td>
                        <td>{{ $timeSheet->total_minutes_grade_4/60  }}</td>
                        <td>{{ $timeSheet->total_minutes_grade_5/60  }}</td>
                        <td></td> 
                        <td><a href="{{route('timeRecords.index', ['id'=>$timeSheet->id])}}" class="btn btn-primary">Show Records</a></td>
                        
                        <td><a href="{{ route('timeSheet.generateTimeSheetsTestPDF') }}?id=1&costcenter=1&colnnovate=true&ktp=true&ptl=true&academy=false&innovation=false" class="btn btn-primary" > Download 3 TimeSheets CC</a></td>
                        <td><a href="{{ route('timeSheet.generateTimeSheetsTestPDF') }}?id=1&colnnovate=false&ktp=true&ptl=true&academy=false&innovation=false" class="btn btn-primary" > Download 2 TimeSheets</a></td>
                        <td><a href="{{ route('timeSheet.generateTimeSheetsTestPDF') }}?id=1&costcenter=1&colnnovate=true&ktp=true&ptl=true&academy=true&innovation=true" class="btn btn-primary" > Download All TimeSheets CC</a></td>
                        <td><a href="{{ route('timeSheet.generateTimeSheetsTestPDF') }}?id=1&colnnovate=true&ktp=true&ptl=true&academy=true&innovation=true" class="btn btn-primary" > Download All TimeSheets</a></td>

                        <td><a href="{{ route('timeSheet.generateColnnovatePDF', ['id'=>$timeSheet->id, 'costCenterid'=>$costCenterid] )  }}" class="btn btn-primary" > Download Colnnovate CC</a></td>
                        <td><a href="{{ route('timeSheet.generateAcademyPDF', ['id'=>$timeSheet->id,'costCenterid'=>$costCenterid]) }}" class="btn btn-primary" > Download Academy CC</a></td>
                        <td><a href="{{ route('timeSheet.generateTimeSheetPDF', ['id'=>$timeSheet->id,'costCenterid'=>$costCenterid]) }}" class="btn btn-primary" > Download Innovation CC</a></td>
                        <td><a href="{{ route('timeSheet.generateKtpPDF', ['id'=>$timeSheet->id,'costCenterid'=>$costCenterid]) }}" class="btn btn-primary" > Download KTP CC</a></td>
                        <td><a href="{{ route('timeSheet.generatePTLPDF', $timeSheet->id) }}" class="btn btn-primary" > Download PTL</a></td>
                        
                        <td><a href="{{ route('timeSheet.generateColnnovatePDF', $timeSheet->id) }}" class="btn btn-primary" > Download Colnnovate</a></td>
                        <td><a href="{{ route('timeSheet.generateAcademyPDF', $timeSheet->id) }}" class="btn btn-primary" > Download Academy</a></td> 
                        <td><a href="{{ route('timeSheet.generateKtpPDF', $timeSheet->id) }}" class="btn btn-primary" > Download KTP</a></td>
                        <td><a href="{{ route('timeSheet.generateTimeSheetPDF', $timeSheet->id) }}" class="btn btn-primary" > Download Innovation</a></td>
                        <td><a href="{{route('timeSheet.edit', $timeSheet->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><form action="{{route('timeSheet.destroy', $timeSheet->id)}}" method="POST">
                        @csrf 
                        @method('DELETE')
                        <button class="btn btn-primary" type="sumbit"> Delete</button></form></td>
                    </tr> 
                @endforeach
            </tbody>
        </table>

		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection