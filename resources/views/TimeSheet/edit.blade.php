@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Existing TimeSheet</h1>
        <div>
	{{-- Error Checking --}}
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('timeSheet.update', $timeSheet->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="month">Month:</label>
                <select name="month">
                    <option value="January">January</option>
                    <option value="January">Febarury</option>
                    <option value="January">March</option>
                    <option value="January">April</option>
                    <option value="January">May</option>
                    <option value="January">June</option>
                    <option value="January">July</option>
                    <option value="January">August</option>
                    <option value="January">September</option>
                    <option value="January">October</option>
                    <option value="January">November</option>
                    <option value="January">December</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $timeSheet->name }}">
            </div>
            <div class="form-group">
                <label for="year">Year:</label>
                <input type="number" class="form-control" name="year" value="{{ $timeSheet->year }}">
            </div>

            <button type="submit" class="btn btn-secondary">Update</button>
        </form>
    
		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection