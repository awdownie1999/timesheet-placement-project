@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Account</h1>
        <div>
	{{-- Error Checking --}}
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <br/>
        <a class="btn btn-primary" href="{{route('account.edit', Auth::user()->id)}}">Edit Account</a>
        <br/> <br/>
            <table>
                <thead>
                    <tr>
                        <th>Name&nbsp;&nbsp;</th>
                        <th>Email&nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{Auth::user()->name}}&nbsp;&nbsp;</td>
                        <td>{{Auth::user()->email}}&nbsp;&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <br/>
            @if(Auth::user()->getMedia('signatures')->first() != null)
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature &nbsp;&nbsp;&nbsp;
                <img src="{{ Auth::user()->getFirstMediaUrl('signatures', 'square') }}">
                <br /><br />
                <form action="{{route('account.destroy', Auth::user()->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    &nbsp;&nbsp;&nbsp;<button class="btn btn-primary" type="sumbit"> Delete</button></form>
            @else
                <form method="POST" enctype="multipart/form-data" action="{{ route('account.store', Auth::user()->id) }}">
                    @csrf
                    <input type="file" name="signature" />
                    <button type="submit" class="button button-primary">Submit</button>
                </form>
            @endif
		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection