@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Records for {{ $timeSheet->name }}</h1>
        <div>
	{{-- Error Checking --}}
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <div><a style="margin:19px;" href="{{route('timeRecords.create', ['id'=>$timeSheet->id])}}" class="btn btn-primary">New Record</a></div>
        <table><thead><tr>
            <th>Month</th>
            <th>&nbsp;&nbsp;Description</th>
            <th>&nbsp;&nbsp;timeIn</th>
            <th>&nbsp;&nbsp;timeOut</th>
            <th>&nbsp;&nbsp;Break</th>
            <th>&nbsp;Hours</th>
            <th>&nbsp;Grade</th>
            <th>&nbsp;Project Code</th>
        </tr></thead><tbody>
        @foreach($timeRecords as $timeRecord)
            <tr>
                <td>&nbsp;&nbsp;{{$timeSheet->month}}</td>
                <td>&nbsp;&nbsp;{{$timeRecord->description}}</td>
                <td>&nbsp;&nbsp;{{$timeRecord->timeIn}}</td>
                <td>&nbsp;&nbsp;{{$timeRecord->timeOut}}</td>
                @if($timeRecord->break == 1)
                    <td>Yes</td>
                @else
                    <td>No</td>
                @endif
                <td>&nbsp;&nbsp;{{$timeRecord->minutes/60}}</td>
                <td>&nbsp;&nbsp;{{$timeRecord->grade->type}}</td>
                <td>&nbsp;{{$timeRecord->costCenter->number}}</td>
                <td>&nbsp;&nbsp;<a href="{{route('timeRecords.edit', $timeRecord->id)}}" class="btn btn-primary">Edit</a></td>
                <td><form action="{{route('timeRecords.destroy', $timeRecord->id)}}" method="POST">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit"> Delete</button></form></td>
            </tr>
        @endforeach</tbody>
        </table>
		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection