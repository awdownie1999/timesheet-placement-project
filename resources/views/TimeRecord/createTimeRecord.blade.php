@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create New Record</h1>
        <div>
	{{-- Error Checking --}}
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <form method="POST" action="{{route('timeRecords.store')}}">
            @csrf
        <input type="hidden" class="form-control" name="timeSheetID" value="{{ $timeSheetID }}"/>
            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description"/>
            </div>
            <div class="form-group">
                <label for="subject">Subject:</label>
                <input type="text" name="subject"/>
            </div>

            <div class="form-group">
                <label for="costCenter">Cost Center:</label>
                <select name="costCenter">
                    @foreach ($costCenters as $costCenter)
                        <option value="{{$costCenter->id}}">{{$costCenter->type}} &nbsp;&nbsp;&nbsp;{{$costCenter->number}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="grade">Grade:</label>
                <select name="grade">
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                </select>
            </div>

            <div class="form-group">
                <label for="timeIn">Time In:</label>
                <input type="datetime-local" class="form-control" name="timeIn"/>
            </div>

            <div class="form-group">
                <label for="timeOut">Time Out:</label>
                <input type="datetime-local" class="form-control" name="timeOut"/>
            </div>

            <div class="form-group">
                <label for="break">Break:</label>
                <input type="checkbox" name="break"/>
            </div>

            <label for="hoursWorked"></label> {{-- JavaScript Here --}}

            <button type="submit" class="btn btn-primary">Create</button>
        </form>

		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection