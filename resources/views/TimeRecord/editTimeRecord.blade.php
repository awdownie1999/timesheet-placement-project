@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Existing TimeSheet</h1>
        <div>
	{{-- Error Checking --}}
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <form method="post" action="{{ route('timeRecords.update', $timeRecord->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="costCenter">Cost Center:</label>
                <select name="costCenter">
                    @foreach ($costCenters as $costCenter)
                        <option value="{{$costCenter->id}}">{{$costCenter->type}} &nbsp;&nbsp;&nbsp;{{$costCenter->number}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="grade">Grade:</label>
                <select name="grade">
                    @foreach ($grades as $grade)
                        <option value="{{$grade->id}}">{{$grade->type}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="timeIn">Time In:</label>
                <label>{{ $timeRecord->timeIn }}</label>
                <input type="datetime-local" class="form-control" name="timeIn" value="{{ $timeRecord->timeIn }}">
            </div>

            <div class="form-group">
                <label for="timeOut">Time Out:</label>
                <label>{{ $timeRecord->timeOut }}</label>
                <input type="datetime-local" class="form-control" name="timeOut" value="{{ $timeRecord->timeOut }}">
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $timeRecord->description }}">
            </div>
            <div class="form-group">
                <label for="break">Break:</label>
                <input type="checkbox" name="checkbox" value="{{ $timeRecord->break }}">
            </div>
            <button type="submit" class="btn btn-secondary">Update</button>
        </form>
    
		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection