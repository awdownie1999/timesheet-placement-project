@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create New Cost Center</h1>
        <div>
        <form method="POST" action="{{route('costCenter.store')}}">
            @csrf
            <div class="form-group">
                <label for="type">type:</label>
                <input type="text" class="form-control" name="type"/>
            </div>

            <div class="form-group">
                <label for="number">Number:</label>
                <input type="number" class="form-control" name="number"/>
            </div>
            <button type="submit" class="btn btn-primary">Create Project</button>
        </form>

		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection