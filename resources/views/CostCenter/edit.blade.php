@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Cost Center</h1>
        <div>
        <form method="post" action="{{ route('costCenter.update', $costCenter->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="type">type:</label>
                <input type="text" class="form-control" name="type" value="{{ $costCenter->type }}">
            </div>
            <div class="form-group">
                <label for="number">Number:</label>
                <input type="number" class="form-control" name="number" value="{{ $costCenter->number }}">
            </div>
            <div class="form-group">
                <label for="users">Users:</label>
                <select name="users">
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-secondary">Update</button>
        </form>

		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection