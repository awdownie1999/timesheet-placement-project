@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Cost Center Index</h1>
        <table>
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Number</th>
                </tr>
            </thead>
            <div><a style="margin:19px;" href="{{route('costCenter.create')}}" class="btn btn-primary">New Cost Center</a></div>
            <tbody>
                @foreach ($costCenter as $costCenter)
                    <tr>
                        <td>{{ $costCenter->type }}</td>
                        <td>{{ $costCenter->number  }}</td>
                        <td><a href="{{route('costCenter.edit', $costCenter->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><form action="{{route('costCenter.destroy', $costCenter->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-primary" type="sumbit"> Delete</button></form></td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection