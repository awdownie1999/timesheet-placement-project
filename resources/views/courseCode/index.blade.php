@extends('layouts.app')

@section('content')

<div class="row">
	{{-- Declare Page Size --}}
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Course Codes</h1>
        <div>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Number</th>
                </tr>
            </thead>
            <div><a style="margin:19px;" href="{{route('courseCode.create')}}" class="btn btn-primary">New Course</a></div>
            <tbody>
                @foreach ($courseCodes as $courseCode)
                    <tr>
                        <td>{{ $courseCode->name  }}</td>
                        <td>{{ $courseCode->number  }}</td>
                        <td><a href="{{route('courseCode.edit', $courseCode->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><form action="{{route('courseCode.destroy', $courseCode->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-primary" type="sumbit"> Delete</button></form></td>
                    </tr> 
                @endforeach
            </tbody>
        </table>


		{{-- Error Checking --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection
