<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseCode extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'name', 'number', 'user_id'
    ];
}
