<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Log;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Account.account');
    }


    /**
     * Store a image for a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('signature') && $request->file('signature')->isValid()){
            Log::info('True');
        }else{
            Log::info('False');
        }

        $user =  User::firstOrNew([
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'password' => Auth::user()->password,
        ]);
    
        if($request->hasFile('signature') && $request->file('signature')->isValid()){
            $user->addMediaFromRequest('signature')->toMediaCollection('signatures');
        }
        Log::info('Image URL' . $user->getMedia('signatures')->first()->getUrl());
        return redirect('/account');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = Auth::user();
        return view('Account.editProfile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
        ]);

        //Update
        $user = Auth::user();
        $user->name = is_null($request->name) ? $user->name : $request->name;
        $user->email = is_null($request->email) ? $user->email : $request->email;
        $user->save();
        return redirect('/account');
    }

    public function destroy($id)
    {
        $userMedia = User::find($id)->getMedia('signatures')->first();
        $userMedia->delete();
        return redirect('/account')->with('deleted');
    }
}

