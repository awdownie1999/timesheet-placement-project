<?php

namespace App\Http\Controllers;

use App\CostCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class CostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costCenter = CostCenter::all();
        return view('CostCenter.index', compact('costCenter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $costCenter = CostCenter::all();
        return view('CostCenter.create', compact('costCenter'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|string',
            'number' => 'required|integer|min:4',
        ]);

        $costCenter = new CostCenter;
        $costCenter->type = $request->type; 
        $costCenter->number = $request->number;
        $costCenter->save();
        return redirect('/costCenter')->with('Created');
        return response()->json([
            "message" => "costCenter created"
        ], 201);
        /*
      

        $costCenter = CostCenter::firstOrNew([
            'type' => $request->get('type'),
            'number' => $request->get('number'),
            'user_id' => Auth::user()->id,
        ]);
        $costCenter->save();

        return redirect('/costCenter')->with('Created');
        */

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function show(CostCenter $costCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(CostCenter $costCenter)
    {
        $users = User::all();
        return view('CostCenter.edit', compact('costCenter', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostCenter $costCenter)
    {
        $request->validate([
            'type' => 'required|string|Max:255',
            'number' => 'required|integer|Min:4',
        ]);
        $costCenter->type = is_null($request->type) ? $costCenter->type : $request->type;
        $costCenter->number = is_null($request->number) ? $costCenter->number : $request->number;
        $costCenter->user_id = is_null($request->user_id) ? $costCenter->user_id : $request->user_id;
        $costCenter->save();
        return redirect('/costCenter');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostCenter $costCenter)
    {
        $costCenter->delete();
        return redirect('/costCenter');
    }
}
