<?php

namespace App\Http\Controllers;

use App\CourseCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class CourseCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courseCodes = CourseCode::all();
        return view('courseCode.index', compact('courseCodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courseCode = CourseCode::all();
        return view('courseCode.create', compact('courseCode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'number' => 'required|integer|min:4',
        ]);

        $courseCode = new CourseCode;
        $courseCode->name = $request->name; 
        $courseCode->number = $request->number; 
        $courseCode->user_id = Auth::user()->id; 
        $courseCode->save();
        return redirect('/courseCode')->with('Created');


        // $courseCode = CourseCode::firstOrNew([
        //     'name' => $request->get('name'),
        //     'user_id' => Auth::user()->id,
        // ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\courseCode  $courseCode
     * @return \Illuminate\Http\Response
     */
    public function show(CourseCode $courseCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\courseCode  $courseCode
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseCode $courseCode)
    {
        $users = User::all();
        return view('courseCode.edit', compact('courseCode', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\courseCode  $courseCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseCode $courseCode)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'number' => 'required|integer|Min:3',
        ]);
        $courseCode->name = is_null($request->name) ? $courseCode->name : $request->name;
        $courseCode->number = is_null($request->number) ? $courseCode->number : $request->number;
        $courseCode->user_id = is_null($request->user_id) ? $courseCode->user_id : $request->user_id;
        $courseCode->save();
        return redirect('/courseCode');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\courseCode  $courseCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseCode $courseCode)
    {
        $courseCode->delete();
        return redirect('/courseCode');
    }
}
