<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TimeSheet;
use App\TimeRecord;
use App\CostCenter;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;
use File;
use ZipArchive;

class TimeSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $emptySearch = 1;
        $queryOrder = "CASE WHEN month = 'January' THEN 1";
        $queryOrder .= " WHEN month = 'February' THEN 2";
        $queryOrder .= " WHEN month = 'March' THEN 3";
        $queryOrder .= " WHEN month = 'April' THEN 4";
        $queryOrder .= " WHEN month = 'May' THEN 5";
        $queryOrder .= " WHEN month = 'June' THEN 6";
        $queryOrder .= " WHEN month = 'July' THEN 7";
        $queryOrder .= " WHEN month = 'August' THEN 8";
        $queryOrder .= " WHEN month = 'September' THEN 9";
        $queryOrder .= " WHEN month = 'October' THEN 10";
        $queryOrder .= " WHEN month = 'November' THEN 11";
        $queryOrder .= " ELSE 12 END";
        $timeSheets = TimeSheet::where('user_id', Auth::user()->id)->orderByRaw($queryOrder)->get();

        if($request->has('q')){
            $q = $request->get( 'q' );
            Log::info('Query Check : ' . $q);
            $timeSheets = TimeSheet::where('name', 'LIKE', '%' . $q . '%')->where('user_id', Auth::user()->id)->orderByRaw($queryOrder)->get();
            if (count( $timeSheets ) == 0) {
                $emptySearch = 0;
            }else{
                $emptySearch = 1;
            }
        }elseif($request->filter == 'all'){
            $timeSheets = TimeSheet::where('user_id', Auth::user()->id)->orderByRaw($queryOrder)->get();
            $emptySearch = 1;
            //$costCenters = CostCenter::all();
        }
        return view('TimeSheet.index', compact('timeSheets', 'emptySearch'));
    }

    public function create()
    {
        $timeSheets = TimeSheet::all();
        return view('TimeSheet.create', compact('timeSheets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'month' => 'required|string',
            'name' => 'required|string',
            'year' => 'required|integer',
        ]);

        $timesheet = new TimeSheet;
        $timesheet->month = $request->get('month');
        $timesheet->name = $request->get('name');
        $timesheet->year = $request->get('year');
        $timesheet->company = $request->get('company');
        $timesheet->user_id = Auth::user()->id;
        $timesheet->save();
        return redirect('/timeSheet')->with('Created');
    }

    public function show(TimeSheet $timeSheet)
    {
    }

    public function edit(TimeSheet $timeSheet)
    {
        return view('TimeSheet.edit', compact('timeSheet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TimeSheet $timeSheet)
    {
        $request->validate([
            'month' => 'required|string',
            'name' => 'required|string',
            'year' => 'required|integer',
        ]);
        //Update
        $timeSheet->name = is_null($request->name) ? $timeSheet->name : $request->name;
        $timeSheet->month = is_null($request->month) ? $timeSheet->month : $request->month;
        $timeSheet->year = is_null($request->year) ? $timeSheet->year : $request->year;
        $timeSheet->save();
        return redirect('/timeSheet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeSheet $timeSheet)
    {
        $timeSheet->delete();
        return redirect('/timeSheet');
    }


    /**
     * Generates a Zip folder with Timesheet PDFs.
     * 
     * Method that when provided Pdf timesheet id's and or a costCenterId will generate the timesheet PDF and downlaod the PDF(s) in a zip folder.
     * All records related to the ID of the timesheet will be rerieved and sorted in a chronological order in a PDF
     * @param   int	$colnnovateid
     * @param  	int $innovationID
     * @param  	int $ktpid
     * @param  	int $ptlid
     * @param  	int $academyid
     * 
     */
    public function generateTimeSheetsPDFDepricated($costCenterid=null, $innovationID=null, $colnnovateid=null, $ktpid=null, $ptlid=null, $academyid=null)
    {
        //global vars
        $user = Auth::user();
        $costCenter = null;

        //setting pdf structure
        $files = array();
        $rootPath = 'public/' . $user->id . '/multipleTs/';
        $colnnovatePdf = null;
        $ptlPdf = null;
        $ktpPdf = null;
        $innovationPdf = null;
        $academyPdf = null;

        //KTP
        $ktimeSheet = null;
        $kTimeRecords = null;

        //Academy
        $atimeRecords = null;

        //colnnovate & innovate
        $timeRecordsArrayInnovation = array();
        $timeRecordsArrayColnnovate = array();
        $iTitle = "";
        $cTitle = "";

        //ptl
        $ptlTimeSheet = null;
        $ptimeRecords = null;
        $timeRecordArrayPTL = array();
        $days = array("MON"=>[], "TUE"=>[], "WED"=>[], "THU"=>[], "FRI"=>[], "SAT"=>[]);
        //Array of Keys
        $weekMap = [
            0 => 'SUN',
            1 => 'MON',
            2 => 'TUE',
            3 => 'WED',
            4 => 'THU',
            5 => 'FRI',
            6 => 'SAT',
        ];
        
        if($innovationID != null){
            $title = "Innovation Voucher Timesheet";
            $timeSheet = TimeSheet::find($innovationID);
            $decision = "timesheet";
            
            if($costCenterid != null){
                $timeRecordsArray = $this->innovationColnnovate($costCenterid, $innovationID);
            }else{
                $timeRecordsArray = $this->innovationColnnovate($innovationID);
            }
            $file_name = 'innovation-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
            
            $pdf = PDF::loadview('Pdf.timesheet', compact('user', 'timeRecordsArray', 'title', 'costCenter', 'innovationID', 'timeSheet', 'decision'))
            ->setPaper('a4', 'landscape')->save($rootPath . $file_name);
           // $this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);
        }

        if($colnnovateid != null){
            $title = " HE - FE PROJECT TIMESHEET";
            $timeSheet = TimeSheet::find($colnnovateid);
            $decision = null;
            
            if($costCenterid != null){
                $timeRecordsArray = $this->innovationColnnovate($costCenterid, $colnnovateid);
            }else{
                $timeRecordsArray = $this->innovationColnnovate($colnnovateid);
            }
            $file_name = 'colnnovate-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
            
            $pdf = PDF::loadview('Pdf.timesheet', compact('user', 'costCenter', 'colnnovateid', 'timeRecordsArray', 'title', 'costCenter', 'timeSheet', 'decision'))
            ->setPaper('a4', 'landscape')->save($rootPath . $file_name);
            //$this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);
        }

        //Academy
        if($academyid != null){
            $aTimeSheet = TimeSheet::find($academyid);
            if($costCenterid != 0 || $costCenterid != null){
                $costCenter = CostCenter::where('id', $costCenterid)->first();
                $atimeRecords = TimeRecord::where('time_sheet_id', $academyid)->where('cost_center_id', $costCenterid)->orderBy('dateOfRecord')->get();  //->groupBy('dateOfRecord);
            }else{
                $atimeRecords = TimeRecord::where('time_sheet_id', $academyid)->orderBy('dateOfRecord')->get();  //->groupBy('dateOfRecord);
            }
            $file_name = 'academy-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
            
            $pdf = PDF::loadview('Pdf.academy', compact('user', 'costCenter', 'academyid',))
            ->setPaper('a4', 'landscape')->save($rootPath . $file_name);
           // $this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);
        }

        //KTP
        if($ktpid != null){
            $kTimeSheet = TimeSheet::find($ktpid);
            if($costCenterid != 0 || $costCenterid != null){
                $costCenter = CostCenter::where('id', $costCenterid)->first();
                $kTimeRecords = TimeRecord::where('time_sheet_id', $ktpid)->where('cost_center_id', $costCenterid)->orderBy('dateOfRecord')->get();
            }else{
                $kTimeRecords = TimeRecord::where('time_sheet_id', $ktpid)->orderBy('dateOfRecord')->get();
            }
            $file_name = 'ktp-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);

            $pdf = PDF::loadview('Pdf.ktp', compact('user', 'costCenter', 'ktpid', 'timeSheet', 'kTimeRecords'))
            ->setPaper('a4', 'portrait')->save($rootPath . $file_name);
            $ptlPdf = $this->downloadPDF($pdf, $file_name);
            //return $ptlPdf;
            array_push($files, $file_name);
        }

        //PTL
        if($ptlid != null){
            $ptlTimeSheet = TimeSheet::find($ptlid);
            $ptimeRecords = TimeRecord::where('time_sheet_id', $ptlid)->get();
            $days = $this->PTl($ptlid, $days, $weekMap, $ptimeRecords);
            
            $file_name = 'ptl-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);

            $pdf = PDF::loadview('Pdf.PTLClaim', compact('user', 'costCenter', 'ptlid', 'ptlTimeSheet', 'days', 'weekMap'))->setPaper('a4', 'landscape')->save($rootPath . $file_name);
           // $this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);

        }
        $zipname = 'allTimesheets.zip';
        $zip = new ZipArchive;
        /*
        $zip->open($zipname, ZipArchive::CREATE);
        foreach ($files as $file) {
            echo $path = $rootPath . $file;
            if(file_exists($path)){
                Log::info('file exists');
                $zip->addFromString(basename($path),  file_get_contents($path)); ;
            }
        }
        $zip->close();
        */
        if($zip->open(public_path($zipname), ZipArchive::CREATE) === TRUE){
            foreach($files as $file){
                $path = $rootPath . $file;
                if(file_exists(public_path($path))){
                    $zip->addFile($path, basename($path));
                }
            }
            $zip->close();
        }

       if(file_exists(public_path($zipname))){
        return response()->download(public_path($zipname))->deleteFileAfterSend(true);
    }else{
           return 'Zip file does not exist.';
       }
    }



/**
     * Generates a Zip folder with Timesheet PDFs.
     * 
     * Method that when provided Pdf timesheet id's and or a costCenterId will generate the timesheet PDF and downlaod the PDF(s) in a zip folder.
     * All records related to the ID of the timesheet will be rerieved and sorted in a chronological order in a PDF
     * @param   int	$colnnovateid
     * @param  	int $innovationID
     * @param  	int $ktpid
     * @param  	int $ptlid
     * @param  	int $academyid
     * 
     */
    public function generateTimeSheetsTestPDF(Request $request)
    {
        //global vars
        $id = $request->get('id');
        $costCenterid = $request->get('costcenter');
        $timeSheet = TimeSheet::find($id);
        $costCenter = CostCenter::find($costCenterid);
        $academy = $request->get('academy');
        $ptl = $request->get('ptl');
        $ktp = $request->get('ktp');
        $colnnovate = $request->get('colnnovate');
        $innovation = $request->get('innovation');
        $user = Auth::user();

        Log::info('id ' . $id);
        Log::info('costcenterID ' . $costCenterid);
        Log::info('academy ' . $academy);
        Log::info('ptl ' . $ptl);
        Log::info('ktp ' . $ktp);
        Log::info('colnnovate ' . $colnnovate);
        Log::info('innovate ' . $innovation);

        //setting pdf structure
        $files = array();
        $rootPath = 'public/' . $user->id . '/multipleTs/';

        //Pdfs
        $kTimeRecords = null;
        $atimeRecords = null;
        $timeRecordsArrayInnovation = array();
        $timeRecordsArrayColnnovate = array();
        $iTitle = "";
        $cTitle = "";

        //ptl
        $ptlTimeSheet = null;
        $ptimeRecords = null;
        $timeRecordArrayPTL = array();
        $days = array("MON"=>[], "TUE"=>[], "WED"=>[], "THU"=>[], "FRI"=>[], "SAT"=>[]);
        //Array of Keys
        $weekMap = [
            0 => 'SUN',
            1 => 'MON',
            2 => 'TUE',
            3 => 'WED',
            4 => 'THU',
            5 => 'FRI',
            6 => 'SAT',
        ];
        
        if($innovation == "true"){
            Log::info('inside innovation if statement');
            $title = "Innovation Voucher Timesheet";
            $decision = "timesheet";
            
            $timeRecordsArray = $this->innovationColnnovate($costCenterid, $id);
            Log::info($timeRecordsArray);
            $file_name = 'innovation-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
            
            $pdf = PDF::loadview('Pdf.timesheet', compact('user', 'timeRecordsArray', 'title', 'costCenter', 'timeSheet', 'decision'))
            ->setPaper('a4', 'landscape')->save($rootPath . $file_name);
           // $this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);
        }

        if($colnnovate == "true"){
            $title = " HE - FE PROJECT TIMESHEET";
            $decision = null;
            
            $timeRecordsArray = $this->innovationColnnovate($costCenterid, $id);
            $file_name = 'colnnovate-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
            
            $pdf = PDF::loadview('Pdf.timesheet', compact('user', 'costCenter', 'timeRecordsArray', 'title', 'costCenter', 'timeSheet', 'decision'))
            ->setPaper('a4', 'landscape')->save($rootPath . $file_name);
            //$this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);
        }

        //Academy
        if($academy == "true"){
            $aTimeSheet = TimeSheet::find($id);
            if($costCenterid != null){
                $atimeRecords = TimeRecord::where('time_sheet_id', $id)->where('cost_center_id', $costCenterid)->orderBy('dateOfRecord')->get();  //->groupBy('dateOfRecord);
            }else{
                $atimeRecords = TimeRecord::where('time_sheet_id', $id)->orderBy('dateOfRecord')->get();  //->groupBy('dateOfRecord);
            }
            $file_name = 'academy-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
            
            $pdf = PDF::loadview('Pdf.academy', compact('user', 'costCenter',))
            ->setPaper('a4', 'landscape')->save($rootPath . $file_name);
           // $this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);
        }

        //KTP
        if($ktp == "true"){
            if($costCenterid != null){
                $kTimeRecords = TimeRecord::where('time_sheet_id', $id)->where('cost_center_id', $costCenterid)->orderBy('dateOfRecord')->get();
            }else{
                $kTimeRecords = TimeRecord::where('time_sheet_id', $id)->orderBy('dateOfRecord')->get();
            }
            $file_name = 'ktp-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);

            $pdf = PDF::loadview('Pdf.ktp', compact('user', 'costCenter', 'timeSheet', 'kTimeRecords'))
            ->setPaper('a4', 'portrait')->save($rootPath . $file_name);
            $ptlPdf = $this->downloadPDF($pdf, $file_name);
            //return $ptlPdf;
            array_push($files, $file_name);
        }

        //PTL
        if($ptl == "true"){
            $ptimeRecords = TimeRecord::where('time_sheet_id', $id)->get();
            $days = $this->PTl($id, $days, $weekMap, $ptimeRecords);
            
            $file_name = 'ptl-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
            File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);

            $pdf = PDF::loadview('Pdf.PTLClaim', compact('user', 'costCenter', 'timeSheet', 'days', 'weekMap'))->setPaper('a4', 'landscape')->save($rootPath . $file_name);
           // $this->downloadPDF($pdf, $file_name);
            array_push($files, $file_name);

        }
        $zipname = "";
        foreach($files as $file){
            $pdf = substr($file, 0, 3) ;
            $zipname .= $pdf;
            Log::info($zipname);
        }
        $zipname .= '.zip';
        Log::info($zipname);

        $zip = new ZipArchive;

        if($zip->open(public_path($zipname), ZipArchive::CREATE) === TRUE){
            foreach($files as $file){
                Log::info('file' . $file);
                $path = $rootPath . $file;
                if(file_exists(public_path($path))){
                    $zip->addFile($path, basename($path));
                }
            }
            $zip->close();
        }

       if(file_exists(public_path($zipname))){
        return response()->download(public_path($zipname))->deleteFileAfterSend(true);
    }else{
           return 'Zip file does not exist.';
       }
    }


    /**
     * Method that when provided a Pdf view and a file name will download the time sheet
     * @param   view $pdf
     * @param  	string $file_name
     * @return  download
     * 
     */
    private function downloadPDF($pdf, $file_name){
        return $pdf->download($file_name);
    }


    /**
     * Method that will return a map and use a key to access its contents.
     * 
     * This will use eloquent to retrieve data from the database based on the parameters passed. 
     * It will loop through the collection created and use a key to access its contents. It will then assign this key to new array 
     * 
     * @param   int $costCenterid
     * @param  	int $PDFID
     * @return  array $timeRecordsArray
     */
    private function innovationColnnovate($costCenterid=null, $timesheetId){
        
        $timeRecordsArray = array();
        if($costCenterid != null){
            $timeRecords = TimeRecord::where('time_sheet_id', $timesheetId)->where('cost_center_id', $costCenterid)->orderBy('dateOfRecord')->get();  //->groupBy('dateOfRecord);
        }else{
            $timeRecords = TimeRecord::where('time_sheet_id', $timesheetId)->orderBy('dateOfRecord')->get();  //->groupBy('dateOfRecord);
        }
        foreach($timeRecords as $timeRecord){
            if(array_key_exists($timeRecord->dateOfRecord, $timeRecordsArray) ){
                $timeRecord->minutes += $timeRecordsArray[$timeRecord->dateOfRecord]->minutes;
                $timeRecord->description .= $timeRecordsArray[$timeRecord->dateOfRecord]->description;
            }
            $timeRecordsArray[$timeRecord->dateOfRecord] = $timeRecord;
            Log::info('timeRecords Array check' .$timeRecordsArray[$timeRecord->dateOfRecord]);
        }
        return $timeRecordsArray;
    }

    /**
     * Method that will return a 3D map and use a key to access its contents.
     * 
     * This will loop through an array of records, get the week number, day of the week and values held within each record. It will
     * make a key from the values and will input a records data to a position using the week number and day of the week. 
     * 
     * @param   array $days
     * @param   array $weekMap
     * @param   array $ptimeRecords
     * @param  	int $ptlId
     * @return  array $days
     * 
     */
    private function PTl($ptlId, $days, $weekMap, $ptimeRecords){
        foreach($ptimeRecords as $timeRecord){
            $recordDate = Carbon::parse($timeRecord->dateOfRecord);
            $weekOfMonth = $recordDate->weekOfMonth;
            $dayOfTheWeek = $recordDate->dayOfWeek;
            $key = trim($timeRecord['subject'] . '-' . $timeRecord['grade']['type'] . '-' . Carbon::parse($timeRecord['timeIn'])->format('H:i') . '-' . Carbon::parse($timeRecord['timeOut'])->format('H:i'));
            Log::info('KEY : ' . $key);
            //if not Sunday
            if($dayOfTheWeek == 0){
                continue;
            }

            if(array_key_exists($key, $days[$weekMap[$dayOfTheWeek]]))
            {
                Log::info('KEY_EXISITS');
                $days[$weekMap[$dayOfTheWeek]][$key]['week_hours'][$weekOfMonth -1] = $timeRecord["minutes"]/60;
            }
            else {
                $days[$weekMap[$dayOfTheWeek]][$key] = [
                    'subject' => $timeRecord['subject'],
                    'course_ref' => 'Academy',
                    'start_time' => Carbon::parse($timeRecord['timeIn'])->format('H:i'),
                    'end_time' => Carbon::parse($timeRecord['timeOut'])->format('H:i'),
                    'week_hours' => [0,0,0,0,0],
                    'cost_center' => $timeRecord->costCenter->number,
                    'grade' => $timeRecord->grade->type,
                ];
                $days[$weekMap[$dayOfTheWeek]][$key]['week_hours'][$weekOfMonth -1] = $timeRecord["minutes"]/60;
                Log::info('KEY_NO_EXISITS');
            }
        }
        $tempDays = $days;
        for($dayIndex = 0; $dayIndex < count($tempDays); $dayIndex++)
        {
            $day = $tempDays[$weekMap[$dayIndex+1]];
            $arr = [];
            $days[$weekMap[$dayIndex+1]] = [];
            foreach($day as $entry)
            {
                array_push($days[$weekMap[$dayIndex+1]], $entry);
            }
        }
        return $days;
    }
}