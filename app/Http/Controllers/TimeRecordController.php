<?php

namespace App\Http\Controllers;

use App\TimeRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\CostCenter;
use Carbon\Carbon;
use App\Grade;
use App\TimeSheet;

class TimeRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info('get id ' . $request->get('id'));
        $currentUser = Auth::user();
        $timeRecords = TimeRecord::monthTimeRecord($request->get('id'));
        $timeSheet = TimeSheet::find($request->get('id'));
        Log::info('timeRecords' . $timeRecords);
        return view('TimeRecord.timeRecordIndex', compact('timeRecords', 'timeSheet'));
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $grades = Grade::all();
        Log::info('get id ' . $request->get('id'));
        $costCenters = CostCenter::all();
        $timeRecords = TimeRecord::all();
        $timeSheetID = $request->get('id');
        $grades = Grade::all();
        return view('TimeRecord.createTimeRecord', compact('costCenters', 'timeRecords', 'timeSheetID', 'grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|string',
            'timeIn' => 'required',
            'timeOut' => 'required',
            'costCenter' => 'required',
        ]);

        $timeIn= Carbon::createFromFormat('Y-m-d\TH:i',$request->get('timeIn'));
        $timeOut= Carbon::createFromFormat('Y-m-d\TH:i',$request->get('timeOut'));
        Log::info('total Minutes' . $timeIn);
        Log::info('total Minutes' . $timeOut);
        
       $minutes = TimeRecord::getHoursWorked($timeIn, $timeOut);
       Log::info('Min' . $minutes);
       
        Log::info('break' . $request->get('break'));
        if($request->get('break') == 'on'){
            $break = true;
        }else{
            $break = false;
        }

        $grade = Grade::firstOrNew([
            'type' => $request->get('grade')
        ]);
        $grade->save();


        $timeRecord = new TimeRecord;
        $timeRecord->time_sheet_id = $request->get('timeSheetID');
        $timeRecord->cost_center_id = $request->get('costCenter');
        $timeRecord->description = $request->get('description');
        $timeRecord->timeOut = $request->get('timeOut');
        $timeRecord->timeIn = $request->get('timeIn');
        $timeRecord->break = $break;
        $timeRecord->grade_id = $grade->id;
        $timeRecord->minutes = $minutes;
        $timeRecord->dateOfRecord = $request->get('timeIn');
        $timeRecord->subject = $request->get('subject');
        $timeRecord->user_id = Auth::user()->id;

        $timeRecord->save();
        return redirect('/timeSheet');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function show(TimeRecord $timeRecord)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeRecord $timeRecord)
    {
        $grades = Grade::all();
        Log::info('timeIN' . $timeRecord->timeIn);
        $costCenters = CostCenter::all();
        return view('TimeRecord.editTimeRecord', compact('timeRecord', 'costCenters', 'grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TimeRecord $timeRecord)
    {
        $request->validate([
            'description' => 'required|string',
        ]);
        if($request->get('timeOut') != null && $request->get('timeOut') != null){
            $timeIn= Carbon::createFromFormat('Y-m-d\TH:i',$request->get('timeIn'));
            Log::info('time in' . $timeIn);
            $timeOut= Carbon::createFromFormat('Y-m-d\TH:i',$request->get('timeOut'));
            $minutes = TimeRecord::hours_worked($timeIn, $timeOut);
            Log::info('Min' . $minutes);
        }
        
        $timeRecord->cost_center_id = is_null($request->cost_center_id) ? $timeRecord->cost_center_id : $request->cost_center_id;
        $timeRecord->description = is_null($request->description) ? $timeRecord->description : $request->description;
        $timeRecord->timeIn = is_null($request->timeIn) ? $timeRecord->timeIn : $request->timeIn;
        $timeRecord->timeOut = is_null($request->timeOut) ? $timeRecord->timeOut : $request->timeOut;
        $timeRecord->grade_id = is_null($request->grade_id) ? $timeRecord->grade_id : $request->grade_id;
        $timeRecord->minutes = is_null($request->minutes) ? $timeRecord->minutes : $request->minutes;
        
        
        $timeRecord->save();
        
        if($request->get('break') == 'on'){
            $timeRecord->break = true;
        }else{
            $timeRecord->break = false;
        }
        $timeRecord->save();
        return redirect('/timeSheet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeRecord $timeRecord)
    {
        $timeRecord->delete();
        return redirect('/timeSheet');
    }
}
