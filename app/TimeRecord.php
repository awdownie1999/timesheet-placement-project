<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon;

class TimeRecord extends Model
{
    public function timeSheet() {
        return $this->belongsTo('App\TimeSheet');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function grade() {
        return $this->belongsTo('App\Grade');
    }
    public function costCenter() {
        return $this->belongsTo('App\CostCenter');
    }
    
    
    public function scopeMonthTimeRecord($query, $timeSheetId){
        Log::info('auth user' . Auth::user()->id);
        Log::info('ID Parameter' . $timeSheetId);
        $timeRecords = TimeRecord::where('user_id', Auth::user()->id)->where('time_sheet_id', $timeSheetId)->get();
        return $timeRecords;
    }

    /**
     * This will return the differnece in minutes between 2 dateTimes  .
     * 
     * @param  timestamp	$timeIn
     * @param  timestamp	$timeOut
     * @return $timeRecords
     */
    public function scopeGetHoursWorked($query, $timeIn, $timeOut){
        Log::info('time in ' . $timeIn);
        Log::info('time out' . $timeOut);
        $totalMinutes = $timeIn->diffInMinutes($timeOut);
        Log::info('totalMinutes ' . $totalMinutes);
        return $totalMinutes;
    }

    /**
     * This will return the differnece in minutes between 2 dateTimes  .
     * 
     * @return $hoursWorked
     */
    public function getHoursWorkedAttribute()
    {
       return $this->timeIn->diffInMinutes($this->timeOut)/60;
    }


    /**
     * This will return the differnece in minutes between 2 dateTimes  .
     * 
     * @param  timestamp	$timeIn
     * @param  timestamp	$timeOut
     * @return [] $val
     */

    public static function hoursWorked($timeIn, $timeOut)
    {
        $newTimeOut = new Carbon\Carbon($timeOut);
        $newTimeIn = new Carbon\Carbon($timeIn);
        $val = $newTimeIn->diffInHours($newTimeOut);
        return $val;
    }

    protected $fillable = [
        'dateOfRecord', 'time_sheet_id', 'cost_center_id', 'description', 'grade_id', 'user_id', 'timeIn', 'timeOut', 'break', 'minutes', 'dateOfRecord', 'subject'
    ];
}
