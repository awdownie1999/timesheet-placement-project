<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{
    public function user() {
        return $this->belongsToMany('App\User');
    }

    public function timeRecord() {
        return $this->hasMany('App\TimeRecord');
    }

    protected $fillable = [
        'type', 'number', 'user_id'
    ];
}
