<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class User extends Authenticatable implements MustVerifyEmail, HasMedia
{
    use Notifiable; use HasMediaTrait;
    
    public function timeRecord() {
        return $this->hasMany('App\TimeRecord');
    }

    public function costCenter() {
        return $this->hasMany('App\CostCenter');
    }

    public function courseCode() {
        return $this->hasMany('App\CourseCode');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'employee_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Alters an image to a look like a square
     *
     * @var array
     */
    public function registerMediaConversions(Media $media = null)
    {        
        $this->addMediaConversion('square')
            ->width(280)
            ->height(312)
            ->sharpen(10);
    }
}
