<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class TimeSheet extends Model
{
    public function timeRecords() {
        return $this->hasMany('App\TimeRecord');
    }
   
    protected $fillable = [
        'month', 'year', 'user_id', 'name', 'company'
    ];

    /**
     * Get total minutes for grade 3
     *
     * @return minutes total
     */
    public function getTotalMinutesGrade3Attribute()
    {
       return $this->timeRecords()->where('grade_id', '=', 1)->sum('minutes');
    }

    /**
     * Get total minutes for grade 4
     *
     * @return minutes total
     */
    public function getTotalMinutesGrade4Attribute()
    {
       return $this->timeRecords()->where('grade_id', '=', 2)->sum('minutes');
    }

    /**
     * Get total minutes for grade 5
     *
     * @return minutes total
     */
    public function getTotalMinutesGrade5Attribute()
    {
       return $this->timeRecords()->where('grade_id', '=', 3)->sum('minutes');
    }

    /**
     * Get total minutes for timesheet 
     *
     * @return minutes total
     */
    public function getTotalMinutesAttribute()
    {
       return $this->timeRecords()->sum('minutes');
    }

}
