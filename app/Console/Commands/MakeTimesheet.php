<?php

// namespace App\Console\Commands;

// use Carbon\Carbon;
// use Illuminate\Console\Command;
// use PDF;
// use File;
// use App\User;
// use App\TimeRecord;
// use App\TimeSheet;
// use Faker;

// class MakeTimesheet extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'timesheet:make';

//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'Creates a timesheet pdf';

//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         $this->info('start of handle');
//         $user =User::find(1);
//         $timeSheet = TimeSheet::find(1);
//         $timeRecordArray = array();
//         $timeRecords = TimeRecord::where('time_sheet_id', $timeSheet->id)->get();

//         foreach($timeRecords as $timeRecord){
//             $timeRecordArray[$timeRecord->dateOfRecord] = $timeRecord;
//             $this->info('print check' .$timeRecord->dateOfRecord);
//         }

//         #$this->info('Array Log ' . dd($timeRecordArray));

//         #$timeRecord= $timeRecordArray['2020-01-01'];
//         $this->info('print hehre ' . $timeRecord->timeIn);

//         $mappedArray = array_keys($timeRecordArray);
//         $this->info('mapped arr' . print_r($mappedArray));

//         # takes the timesheet.blade.php and converts to pdf.
//         $file_name = 'timesheet-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
//         $this->info('past $file name declartion');
//         // $file_name = 'timesheet.pdf';
//         $rootPath = 'storage/' . $user->id . '/timesheet/';
//         File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
//         $pdf = PDF::loadView('Pdf.timesheet', compact('user', 'timeRecords', 'timeSheet', 'timeRecordArray'))->setPaper('a4', 'landscape')->save($rootPath . $file_name);
//         $this->info('saved file name: ' . $file_name . ' as Timesheet');
//     }

// }
