<?php

// namespace App\Console\Commands;

// use Carbon\Carbon;
// use Illuminate\Console\Command;
// use PDF;

// class MakeTimesheetNumber2 extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'timesheetnum2:make';

//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'Creates a timesheet pdf';

//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         # takes the timesheet.blade.php and converts to pdf.
//         $file_name = 'timesheet-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
//         // $file_name = 'timesheet.pdf';
//         $pdf = PDF::loadView('Pdf.timesheetnum2')->setPaper('a4', 'landscape')->save('storage/timesheetnum2/' . $file_name);
//         $this->info('Saved file name: ' . $file_name . ' as TimeS Num 2');
//     }
// }
