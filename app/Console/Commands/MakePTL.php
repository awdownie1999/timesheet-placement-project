<?php

// namespace App\Console\Commands;

// use Carbon\Carbon;
// use Illuminate\Console\Command;
// use PDF;
// use File;
// use App\User;
// use App\TimeRecord;
// use App\TimeSheet;
// use Faker;
// use Illuminate\Support\Facades\Hash;

// class MakePTL extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'ptl:make';

//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'Creates a ptl pdf';

//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         $this->info('start of handle');
//         $user =User::find(1);
//         $this->info('user : ' . $user);
//         $timeSheet = TimeSheet::find(1);
//         $timeRecords = TimeRecord::where('time_sheet_id', $timeSheet->id)->orderBy('timeIn', 'DESC')->get();



//         $array1 = array("fruit"=>"oranges", "treeFruit"=>"apples");
//         $array2 = array("treeFruit"=>"pears", "fruit"=>"grapes");
//         $array3 = array("treeFruit"=>"bananas", "fruit"=>"strawberries");
//         $twoDArrayHolder = array('arr1'=>$array1, 'arr2'=>$array2, 'arr3'=>$array3);
//         #$this->info('2D Array' . print_r($twoDArrayHolder));

//         $weeks = array("week1"=>[], "week2"=>[], "week3"=>[], "week4"=>[], "week5"=>[]);
//         $days = array("MON"=>$weeks, "TUE"=>$weeks, "WED"=>$weeks, "THU"=>$weeks, "FRI"=>$weeks, "SAT"=>$weeks, "SUN"=>$weeks);
//         $this->info('2D Array' . print_r($days));

//         $weekMap = [
//             0 => 'SUN',
//             1 => 'MON',
//             2 => 'TUE',
//             3 => 'WED',
//             4 => 'THU',
//             5 => 'FRI',
//             6 => 'SAT',
//         ];

//         foreach($timeRecords as $timeRecord){

//             $recordDate = Carbon::parse($timeRecord->dateOfRecord);
//             $this->info('Record Date ' . $recordDate);
//             $weekOfMonth = $recordDate->weekOfMonth;
//             $this->info('Week ' . $weekOfMonth);
//             $dayOfTheWeek = $recordDate->dayOfWeek;
//             $this->info('Day of week ' . $dayOfTheWeek);

//             if($dayOfTheWeek == 1){
//                 continue;
//             }

//             $tempWeeks = $days[$weekMap[$dayOfTheWeek]];
//             $this->info($weekMap[$dayOfTheWeek]);


//             $sectionOfDay = $tempWeeks['week'.$weekOfMonth];
//             array_push($sectionOfDay, $timeRecord);
//             $tempWeeks['week'.$weekOfMonth] = $sectionOfDay;
//             $this->info('WeekDay ' . print_r($tempWeeks));

//             $days[$weekMap[$dayOfTheWeek]] = $tempWeeks;
//             $this->info('Check Days Arr ' . print_r($days));

//         }

//         # takes the timesheet.blade.php and converts to pdf.
//         $file_name = 'ptl-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
//         $this->info('past $file name declartion');
//         // $file_name = 'timesheet.pdf';
//         $rootPath = 'storage/' . $user->id . '/ptl/';
//         File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
//         $pdf = PDF::loadView('Pdf.PTLClaim', compact('user', 'timeRecords', 'days'))->setPaper('a4', 'landscape')->save($rootPath . $file_name);
//         $this->info('saved file name: ' . $file_name . ' as ptl');
//     }
// }
