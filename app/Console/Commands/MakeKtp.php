<?php

// namespace App\Console\Commands;

// use Carbon\Carbon;
// use Illuminate\Console\Command;
// use PDF;
// use File;
// use App\User;
// use App\TimeRecord;
// use App\TimeSheet;
// use Faker;
// use Illuminate\Support\Facades\Hash;

// class MakeKtp extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'ktp:make';

//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'Creates a Ktp pdf';

//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         $this->info('start of handle');
//         $user =User::find(1);
//         $this->info('user : ' . $user);
//         $timeSheet = TimeSheet::find(1);
//         $timeRecords = TimeRecord::where('time_sheet_id', $timeSheet->id)->orderBy('dateOfRecord')->get();
//         $this->info('time Records : ' . $timeRecords);
//  # takes the timesheet.blade.php and converts to pdf.
//         $file_name = 'ktp-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
//         $this->info('past $file name declartion');
//         // $file_name = 'timesheet.pdf';
//         $rootPath = 'storage/' . $user->id . '/ktp/';
//         File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
//         $pdf = PDF::loadView('Pdf.ktp', compact('user', 'timeRecords', 'timeSheet'))->setPaper('a4', 'portrait')->save($rootPath . $file_name);
//         $this->info('saved file name: ' . $file_name . ' as Ktp');
//     }
// }
