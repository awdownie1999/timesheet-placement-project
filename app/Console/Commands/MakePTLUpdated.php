<?php

// namespace App\Console\Commands;

// use Carbon\Carbon;
// use Illuminate\Console\Command;
// use PDF;
// use File;
// use App\User;
// use App\TimeRecord;
// use App\TimeSheet;
// use Faker;
// use Illuminate\Support\Facades\Hash;

// class MakePTLUpdated extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'newptl:make';

//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'Creates a ptl pdf';

//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         $this->info('start of handle');
//         $user =User::find(1);
//         $this->info('user : ' . $user);
//         $timeSheet = TimeSheet::find(1);
//         $timeRecords = TimeRecord::where('time_sheet_id', $timeSheet->id)->orderBy('timeIn', 'DESC')->get();

//         $days = array("MON"=>[], "TUE"=>[], "WED"=>[], "THU"=>[], "FRI"=>[], "SAT"=>[]);
//         $this->info('2D Array' . print_r($days));

//         $weekMap = [
//             0 => 'SUN',
//             1 => 'MON',
//             2 => 'TUE',
//             3 => 'WED',
//             4 => 'THU',
//             5 => 'FRI',
//             6 => 'SAT',
//         ];
//          // foreach($timeRecords as $timeRecord){

//         //     $recordDate = Carbon::parse($timeRecord->dateOfRecord);
//         //     $this->info('Record Date ' . $recordDate);
//         //     $weekOfMonth = $recordDate->weekOfMonth;
//         //     $this->info('Week ' . $weekOfMonth);
//         //     $dayOfTheWeek = $recordDate->dayOfWeek;
//         //     $this->info('Day of week ' . $dayOfTheWeek);

//         //     if($dayOfTheWeek == 0){
//         //         continue;
//         //     }

//         //     $tempWeeks = $days[$weekMap[$dayOfTheWeek]];
//         //     $this->info($weekMap[$dayOfTheWeek]);

//         //     $sectionOfDay = $tempWeeks['week'.$weekOfMonth];
//         //     array_push($sectionOfDay, $timeRecord);
//         //     $tempWeeks['week'.$weekOfMonth] = $sectionOfDay;

//         //     $days[$weekMap[$dayOfTheWeek]] = $tempWeeks;
//         // }

//         //   ['subject' => 'Java', 'course_ref' => 'Academy', 'start_time' => '09:00', 'end_time' => '11:00',  'week_hours' => [2,0,0,0,0],
//         // 'cost_center' => rand(1, CostCenter::count()), 'grade' => rand(3, 5)],

//             foreach($timeRecords as $timeRecord){

//                 $recordDate = Carbon::parse($timeRecord->dateOfRecord);
//                 $this->info('Record Date ' . $recordDate);
//                 $weekOfMonth = $recordDate->weekOfMonth;
//                 $this->info('Week ' . $weekOfMonth);
//                 $dayOfTheWeek = $recordDate->dayOfWeek;
//                 $this->info('Day of week ' . $dayOfTheWeek);

//                 if($dayOfTheWeek == 0){
//                     continue;
//                 }

//                 switch($dayOfTheWeek){

//                     case 1:
//                         array_push($days, $timeRecord);

//                         break;

//                     case 2:
//                         break;

//                     case 3:
//                         break;

//                     case 4:
//                         break;

//                     case 5:
//                         break;

//                     case 6:
//                         break;

//                     default:
//                         $this->info('Error ');
//                         break;
//                 }

//             }


//         $this->info('Check Days Arr ' . print_r($days));

//         # takes the timesheet.blade.php and converts to pdf.
//         $file_name = 'ptl-' . Carbon::now()->format('d-m-Y-H-i-s') .'.pdf';
//         $this->info('past $file name declartion');
//         // $file_name = 'timesheet.pdf';
//         $rootPath = 'storage/' . $user->id . '/ptl/';
//         File::isDirectory($rootPath) or File::makeDirectory($rootPath, 0777, true, true);
//         $pdf = PDF::loadView('Pdf.PTLClaim', compact('user', 'timeRecords', 'days'))->setPaper('a4', 'landscape')->save($rootPath . $file_name);
//         $this->info('saved file name: ' . $file_name . ' as ptl');
//     }
// }
