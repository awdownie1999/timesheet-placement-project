<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $fillable = [
        'type'
    ];
    
    public function timeRecord() {
        return $this->hasMany('App\TimeRecord');
    }
}
